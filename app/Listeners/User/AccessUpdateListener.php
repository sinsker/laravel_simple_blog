<?php

namespace App\Listeners\User;

use App\Events\LoginEvent;
use App\Module\UserSession;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

//유저 로그인시 최신화
class AccessUpdateListener implements ShouldQueue
{

    use UserSession;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoginEvent  $event
     * @return void
     */
    public function handle(LoginEvent $event)
    {
        $model = $event->getUser();
        $model->access_at = date('Y-m-d H:i:s');
        $model->save();
    }
}
