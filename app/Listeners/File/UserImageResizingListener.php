<?php

namespace App\Listeners\File;

use App\Events\JoinNotifyPushEvent;
use App\Events\UserImageUploadEvent;
use App\Module\UserSession;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Image;

class UserImageResizingListener implements ShouldQueue
{

    use UserSession;

//    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Handle the event.
     *
     * @param  JoinNotifyPushEvent $event
     * @return void
     */
    public function handle(UserImageUploadEvent $event)
    {

        $fileName = $event->getFileName();

        $image = \Intervention\Image\Facades\Image::make($fileName);

        $image->fit(300,300,function($constraint){
            $constraint->upsize();
        });

        $image->save();

        Log::info(' 이미지를 fit 하였습니다.');

    }
}
