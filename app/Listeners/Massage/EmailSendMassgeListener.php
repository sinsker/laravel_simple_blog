<?php

namespace App\Listeners;

use App\Events\JoinNotifyPushEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class EmailSendMassgeListener implements ShouldQueue
{

//    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Handle the event.
     *
     * @param  JoinNotifyPushEvent $event
     * @return void
     */
    public function handle(JoinNotifyPushEvent $event)
    {

        $model = $event->getUser();

        Mail::send('emails.join', $model, function($message) use($model) {

            $subject = $model['name'].' 님 Kanaph 사이트를 가입하신것을 환영합니다.';
            $message->to($model['email'])->subject($subject);
        });


        Log::info( $model['name'].' 님에게 메일 발송을 하였습니다.');
    }

}
