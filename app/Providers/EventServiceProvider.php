<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        //회원가입시 알림 트리거
        'App\Events\JoinNotifyPushEvent' => [
            'App\Listeners\EmailSendMassgeListener',
        ],
        //로그인시 이벤트 트리거
        'App\Events\LoginEvent' => [
            'App\Listeners\User\AccessUpdateListener',
            'App\Listeners\User\UserAccessLogListener',
        ],

        //유저 프로필 사진 변경 트리거
        'App\Events\UserImageUploadEvent' => [
            'App\Listeners\File\UserImageResizingListener'
        ]

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();


    }
}
