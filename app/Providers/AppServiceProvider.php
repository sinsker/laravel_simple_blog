<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        date_default_timezone_set('Asia/Seoul');

        require app_path('Helpers/Common.php');
        //DB 쿼리

        /*\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
            dump([
                'sql' =>  $query->sql,
                'bindings' =>  $query->bindings,
                'time' =>  $query->time,
            ]);
        });*/
    }
}
