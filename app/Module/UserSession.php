<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-29
 * Time: 오전 11:56
 */

namespace App\Module;

use App\Model\User;
use Illuminate\Support\Facades\Auth;

trait UserSession
{

    //유저 세션 저장하기
    function setUserSession(User $user = null)
    {
        $config = config('kanaph');

        if(empty($user))
        {
            $user = Auth::user();
        }

        session(['phone'=>$user->phone]);
        session(['part' =>$config['part'][$user->part]]);
        session(['group'=>['level'=>$user->group_idx, 'name'=>$config['group'][$user->group_idx]]]);

        if( $user->userProfile()->get()->count() > 0)
        {
            $profile = $user->userProfile()->get()[0];

            session(['user_img'=>$profile->img]);
            session(['birth'=>$profile->birth]);
        }

    }

    function getUserSession()
    {
        return;
    }

}