<?php

namespace App\Module;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

trait DataTypeConverter
{

    //리턴 타입 인자값 [query_string]
    static  $DATE_TYPE_QUERY_STRING = '_type';

    //리턴 타입 인자값 [query_string]
    static  $ONLY_DATA_QUERY_STRING = '_data_only';

    protected $originDatas = [];

    protected $conversionDatas = [];


    public function returnView(array $datas, $view = '')
    {

        $this->originDatas = $datas;


        if( !is_null(Input::get(self::$DATE_TYPE_QUERY_STRING)) )
        {
            $dataOnly = !is_null(Input::get(self::$ONLY_DATA_QUERY_STRING)) != '' ? true : false;

            $this->conversionDatas = $this->dataConvert($datas, Input::get(self::$DATE_TYPE_QUERY_STRING));

            return $this->view($view, $dataOnly);
        }

        return view($view, $this->originDatas);
    }


    public function dataConvert(array $datas, $type = 'json')
    {

            switch ($type)
            {
                case 'json':
                    $datas =  json_encode($datas);
                    break;

                case 'xml':
                    $datas = $datas;
                    break;

                case 'html':
                    $datas = $datas;
                    break;

                default:
                    $datas = $datas;
                    break;
            }

        return $datas;
    }

    private function view($view, $dataOnly = false)
    {

        if($dataOnly === true || $view == '')
        {
            return response($this->conversionDatas);
        }

        return response()->view($view, $this->conversionDatas);
    }

}