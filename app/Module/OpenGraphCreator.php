<?php

namespace App\Module;

class OpenGraphCreator
{

    private static $instance = null;

    public $openGraphArray = [];

    private function __construct()
    {
    }

    public static function init()
    {

        return (OpenGraphCreator::getInstance())->put('site','KANAPH')->put('type','blog')->put('url',url($_SERVER['REQUEST_URI']))->put('img',url('/images/kanaph.jpg'));
    }

    public static function getInstance(){
        if(static::$instance === null){
            static::$instance = new OpenGraphCreator();
        }

        return static::$instance;
    }


    //save openGraph
    public function put($key, $value)
    {
        $this->openGraphArray[$key] = $value;

        return $this;
    }

    public function get($key)
    {
        return $this->openGraphArray[$key];
    }

    public function title($title)
    {
        $this->put('title',$title);

        return $this;
    }

    public function  description($description, $len = 40, $encoding = 'UTF-8')
    {
        $description = strip_tags($description);

        if(mb_strlen($description,$encoding) < $len){
            $this->put('description',$description);
        }else{
            $this->put('description',mb_substr($description,'0',$len, $encoding) . '...');
        }

        return $this;
    }

    public function img($img)
    {
        $this->put('img', $img);

        return $this;
    }

    public function type($type)
    {
        $this->put('type', $type);

        return $this;
    }

    public function all($json = false)
    {
        if($json !== false) return json_encode($this->openGraphArray);

        return $this->openGraphArray;
    }

    public function metaHTML($property, $content)
    {
      return '<meta property="og:'.$property.'" content="'.$content.'" >';
    }

    public function template()
    {
        $html = '';

        foreach($this->openGraphArray as $key => $item)
        {
            $html .= $this->metaHTML($key, $item).'
            ';
        }

        return $html;
    }

}