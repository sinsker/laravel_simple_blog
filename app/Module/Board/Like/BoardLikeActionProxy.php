<?php

namespace App\Module\Board\Like;

use App\Model\BoardLike;
use Illuminate\Support\Facades\Auth;

//게시글 좋아요 기능
class BoardLikeActionProxy implements BoardLikeable
{

    private $boardLikeAction;

    //좋아요 등록
    public function boardLike(array $data)
    {

        return BoardLike::create([
            'board_idx' => $data['board_idx'],
            'user_idx'  => Auth::id(),
            'type'      => isset($data['type']) ? $data['type'] : 'love'
        ]);

    }

    //좋아요 취소
    public function boardUnlike(array $data)
    {
        BoardLike::where('user_idx',Auth::id())->where('board_idx',$data['board_idx'])->delete();

    }

    //한 게시글의 좋아요 개수
    public function boardLikeCount(array $data)
    {
        return $this->proxySubjectLode()->boardLikeCount($data);
    }

    //접속 유저의 좋아요 여부
    public function &isLikeUser(array $data = [])
    {
        return $this->proxySubjectLode()->isLikeUser($data);

    }

    //프록시 RealSubject Load
    public function proxySubjectLode()
    {
        if(empty($this->boardLikeAction))
            $this->boardLikeAction = new BoardLikeAction();

        return $this->boardLikeAction;
    }

}