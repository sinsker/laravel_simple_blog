<?php

namespace App\Module\Board\Like;


interface BoardLikeable
{

    public function boardLike(array $data);

    public function boardUnlike(array $data);

    public function boardLikeCount(array $data);

    public function &isLikeUser(array $data = []);

}