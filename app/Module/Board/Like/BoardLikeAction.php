<?php

namespace App\Module\Board\Like;

use App\Model\BoardLike;
use Illuminate\Support\Facades\Auth;

//게시글 좋아요 기능
class BoardLikeAction implements BoardLikeable
{

    //좋아요 등록
    public function boardLike(array $data)
    {

        return BoardLike::create([
            'board_idx' => $data['board_idx'],
            'user_idx'  => Auth::id(),
            'type'      => isset($data['type']) ? $data['type'] : 'love'
        ]);

    }

    //좋아요 취소
    public function boardUnlike(array $data)
    {
        BoardLike::where('user_idx',Auth::id())->where('board_idx',$data['board_idx'])->delete();

    }

    //한 게시글의 좋아요 개수
    public function boardLikeCount(array $data)
    {
        return BoardLike::where('board_idx',$data['board_idx'])->count();

    }

    //접속 유저의 좋아요 여부
    public function &isLikeUser(array $data = [])
    {

        $list = [];

        $data['board_idx'] = isset($data['board_idx']) ? $data['board_idx'] : '';

        $likes = BoardLike::when($data['board_idx'], function ($query) use ($data){
            return $query->where('board_idx',$data['board_idx']);
        })->where('user_idx',Auth::id())->get();

        foreach($likes as $like)
        {
            $list[$like->board_idx] = [
                'user_idx'  => $like->user_idx,
                'type'      => $like->type
            ];
        }

        return $list;
    }


}