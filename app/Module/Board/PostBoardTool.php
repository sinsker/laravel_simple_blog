<?php

namespace App\Module\Board;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait PostBoardTool
{

    static $TagPreg = '(<[\/]?(div|strong|p).*?>)+'; //일반 Html 태그

    static $TagImgPreg = '(<img.+\/>)'; //이미지 태그
    
    static $OneDay = 3600 * 24;

    static $APM = ['am'=>'오전', 'pm'=>'오후'];

    protected $redirectTo = 'kanaph/bbs';


    // 게시글 커스텀 마이징
    protected function cover(&$list, \Closure $callback = null)
    {

        $lr = $list->all();

        foreach($lr as $key => $item)
        {
            //이미지 별도로 배열에 추가
            preg_match('/'.static::$TagImgPreg.'/i', $item->content, $matches, PREG_OFFSET_CAPTURE);

            if(!empty($matches[0][0]))
            {
                $item->src = $matches[0][0];
            }

            //이미지 태그 제거
            $item->content = preg_replace('/('.static::$TagImgPreg.')/i', "", $item->content);

            //띄어쓰기 제거
            $item->content = str_replace('&nbsp;', '', $item->content);

            //날짜 포맷
            $item->date = $this->dateFormatKo($item->reg_date);

            $item = $callback !== null ? $callback($item) : $item;
        }

    }

    //글자 줄이기
    protected function shortcutText($text, $len, $encoding = 'UTF-8')
    {
        if(mb_strlen($text,$encoding) < $len) return $text;

        return mb_substr($text,'0',$len, $encoding) . '...';
    }

    //html 태그 포맷
    public function htmlTagFormat($origin, $input, $replace = '' )
    {
        $input = is_array($input) ? implode('|',$input) : $input;


        $preg = '(<[\/]?('.$input.').*?>)+';

        return  preg_replace('/('.$preg.')/i', $replace, $origin);
    }


    //스킨
    public function skin($bbs_type)
    {
        $bbsConfig = config('bbs.type');

        return $this->redirectTo.'/'.$bbsConfig[$bbs_type]['view'];
    }

    //날짜 포맷
    public function dateFormatKo($date, $default = true)
    {
        $timestamp = strtotime($date);

        $times = time() - $timestamp;

        $day = floor($times / static::$OneDay);

        if($default){
            if($day < 1){
                //$format = static::$APM[date('a', $timestamp)].' '.date('H시 i분',$timestamp);

                $hour = floor($times / 3600);

                if($hour == 0){
                    $min = floor($times / 60 );
                    return $min.'분전';
                }

                $format = $hour.'시간전';
            }else if($day < 2) {
                $format = '하루전';
            }else if($day < 3){
                $format =  '이틀전';
            }else{
                $format = $this->dateFormatKoDefalut($timestamp);
            }
            return $format;
        }

        return $this->dateFormatKoDefalut($timestamp);

    }

    public function dateFormatKoDefalut($timestamp)
    {
        $week = config('kanaph.week');

        return date('Y년 m월 d일 ',$timestamp).' ('.$week[date('w',$timestamp)].')';
    }

}