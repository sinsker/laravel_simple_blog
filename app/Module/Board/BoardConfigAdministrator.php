<?php

namespace App\Module\Board;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait BoardConfigAdministrator
{

    //config list
    public function configList($board)
    {
        return DB::table($board)->select(['*'])->paginate(10);
    }

    //cofig save
    public function store(Request $request)
    {

        $this->validator($request->all())->validate();

        return $this->create($request->all());

    }


    public function update($idx, Request $request)
    {
        return $this->edit($idx, $request->all());
    }



}