<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-14
 * Time: 오후 1:21
 */

namespace App\Module\Board;

use App\Model\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


trait TagManager
{

    public function __construct()
    {

    }

    //태그 배열로 가져오기
    public function tagList($stringTag, $split = ',')
    {
        $tagArray = array();

        if(strpos($stringTag, $split) !== false)
        {
            $splitCateArr = explode($split, $stringTag);

            foreach($splitCateArr as $key => $cate){
                if(empty($cate)) continue;

                $tagArray[] = $cate;
            }

            return $tagArray;
        }

        $tagArray[] = $stringTag;

        return $tagArray;
    }

    //태그 자동 추가
    public function autoTagDataUpdata($datas)
    {
        $tagList = $this->tagList($datas['tag']);

        Tag::where('bc_idx',$datas['bc_idx'])
            ->where('location',$datas['idx'])
            ->delete();

        foreach($tagList as $tag)
        {
            $this->addTagData(['id'=>$tag,'bc_idx'=>$datas['bc_idx'],'location'=>$datas['idx']]);
        }
    }

    //태그 추가
    public function addTagData(array $datas)
    {
        return Tag::create([
                'id'        => $datas['id'],
                'bc_idx'    => $datas['bc_idx'],
                'location'  => $datas['location'],
                'writer'    => Auth::user()['email'],
                'writer_idx'=> Auth::id()
        ]);
    }

    //태그 집합체
    public function tagAssembly($bc_idx = '',$id = '')
    {
        return DB::table('tag')->when(!empty($id), function ($query) use ($id) {
            return $query->where('location', $id);
        })->when($bc_idx, function ($query) use ($bc_idx) {
            return $query->where('bc_idx', $bc_idx);
        })->select(['id',DB::raw('count(id) as tag_cnt')])->groupBy('id')->get();
    }

    //태그 검색
    public function tagSearch($tag)
    {
        return DB::table('tag')
            ->join('board', 'location', '=', 'board.idx')
            ->orderBy('board.bc_idx', 'asc')
            ->orderBy('board.reg_date', 'desc')
            ->where('id', $tag)
            ->select(['id','idx','board.bc_idx','cate','title','content','board.writer','board.writer_idx','hit','reg_date'])->get();
    }

}