<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-27
 * Time: 오후 2:17
 */

namespace App\Module;


use Illuminate\Support\Facades\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

trait JWTUserAuth
{



    public function getJWTAuth()
    {


    }

    public function setJWTAuth(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $token = JWTAuth::attempt($credentials);


        return response()->json(['result'=>$token]);
    }

}