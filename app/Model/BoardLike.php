<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BoardLike extends Model
{
    protected $table = 'board_like';

    protected $primaryKey = ['board_idx', 'user_idx'];

    protected $fillable = ['board_idx', 'user_idx'];

    public $incrementing = false;

    public $timestamps = false;


}