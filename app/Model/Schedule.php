<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Schedule extends Model
{

    protected $table = 'schedule';

    protected $primaryKey = 'idx';

    protected $fillable = ['start','end','option','s_title','s_content'];

}
