<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Board extends Model
{

    protected $table = 'board';

    protected $primaryKey = 'idx';

    protected $fillable = ['pidx','dept','bc_idx','title','notice','cate','tag', 'content','writer','writer_idx'];

    const CREATED_AT = 'reg_date';

    const UPDATED_AT = 'up_date';


    // user Model 과 1;N 관계 정립
    public function user()
    {
        return $this->belongsTo('App\Model\User','writer_idx');
    }

    // boardConfig Model 과 1;N 관계 정립
    public function boardConfig()
    {
        return $this->belongsTo('App\Model\BoardConfig','bc_id');
    }

    //리플과 N:1 관계
    public function replys(){

        return $this->hasMany('App\Model\Reply','bidx');
    }


    public function addHitCount($idx)
    {
        //hit 1 증가
        DB::update('update board set hit = hit+1 where idx =?', [$idx]);

    }
}
