<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BoardConfig extends Model
{

    protected $table = 'board_config';

    protected $primaryKey = 'idx';

    protected $fillable = ['bc_name','bc_row','bc_cate','bc_info'];

    public $timestamps = false;

}
