<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'file';

    protected $primaryKey = 'idx';

    protected $fillable = ['idx','table','table_idx','path','name','oriname','type','size', 'reg_date'];

    public $timestamps = false;

}
