<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tag';

    protected $primaryKey = 'id';

    protected $fillable = ['id','location','writer','writer_idx','bc_idx'];


    // user Model 과 1;N 관계 정립
    public function user()
    {
        return $this->belongsTo('App\Model\User','writer_idx');
    }

    // user Model 과 1;N 관계 정립
    public function board()
    {
        return $this->belongsTo('App\Model\Board','location');
    }
}
