<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $table = 'reply';

    protected $primaryKey = 'idx';

    protected $fillable = ['bidx','content','writer','writer_idx'];

    const CREATED_AT = 'reg_date';

    const UPDATED_AT = 'up_date';


    // user Model 과 1;N 관계 정립
    public function user()
    {
        return $this->belongsTo('App\Model\User','writer_idx');
    }


}
