<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'user';

    protected $primaryKey = 'idx';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'oauth_type', 'part', 'phone', 'email', 'group_idx', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    //관계 정의 1:N
    public function boards(){

        return $this->hasMany('App\Model\Board','writer_idx');
    }

    //관계 정의 1:N
    public function userProfile(){

        return $this->hasMany('App\Model\UserProfile','user_idx');
    }
}
