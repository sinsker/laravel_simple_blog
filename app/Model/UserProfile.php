<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserProfile extends Authenticatable
{
    use Notifiable;

    protected $table = 'user_profile';

    protected $primaryKey = 'user_idx';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_idx', 'img', 'birth', 'job', 'info',
    ];



    //관계 정의 1:1
    public function user(){

        return $this->belongsTo('App\Model\User','user_idx');
    }
}
