<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Model\User;

class UserImageUploadEvent
{
    use InteractsWithSockets, SerializesModels;

    private $fileName;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($fileName)
    {
        $this->setFileName($fileName);
    }


    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
