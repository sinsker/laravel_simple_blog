<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-21
 * Time: 오후 3:11
 */



//파일 사이즈

if(!function_exists('file_size'))
{

    function file_size($size) {
        $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
        if ($size == 0) {
            return('n/a');
        } else {
            return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]);
        }
    }

}


if(!function_exists('file_extration'))
{
    function file_extration($extensions)
    {
        $return = false;
        if (strlen($extensions) === strcspn($extensions, "\\/:*?\"'<>|\n\t\r\x0\x0B")) {
            if (false !== strpbrk($extensions, '.')) {
                $return = strtolower(trim(
                    substr($extensions, strcspn($extensions, '.')),
                    '. '));

                if (false !== strpbrk($return, '.')) {
                    return trim(strrchr($return, '.'), '. ');
                }
            }
        }
        return $return;
    }
}

if(!function_exists('profile_img'))
{
    function profile_img($img = '')
    {

        preg_match('/(http:\/\/|https:\/\/)/i',$img, $matches);

        //return $matches;

        if($img == '') return '/images/kanaph/user.png';

        if(empty($matches))
        {
            return  '/'.$img.'?'.time();
        }

        return $img;
    }
}


if(!function_exists('strpos_array'))
{
    function strpos_array($haystack, $needles) {
        if ( is_array($needles) ) {
            foreach ($needles as $str) {
                if ( is_array($str) ) {
                    $pos = strpos_array($haystack, $str);
                } else {
                    $pos = strpos($haystack, $str);
                }
                if ($pos !== false) {
                    return $pos;
                }
            }
        } else {
            return strpos($haystack, $needles);
        }
    }

}