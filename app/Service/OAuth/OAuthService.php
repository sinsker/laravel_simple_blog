<?php

namespace App\Service\OAuth;


interface OAuthService
{

    //로그인
    public function login($datas = []);

    //회원가입
    public function register($datas = []);

    //API 호출
    public function call($api, $method, $datas = []);

    public function authorizationCheck();

    public function setAccessToken($accessToken);

}