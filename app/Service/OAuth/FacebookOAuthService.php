<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-30
 * Time: 오후 4:41
 */

namespace App\Service\OAuth;


use App\Model\User;
use App\Model\UserProfile;
use App\Module\UserSession;
use Facebook\Facebook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FacebookOAuthService implements OAuthService
{

    use UserSession;

    private $acceccToken = null;


    const OAUTH_TYPE = 'facebook';

    private $fb;


    public function __construct(Request $request = null)
    {
        $this->fb = new Facebook([
            'app_id'                => config('oauth.facebook.id'),
            'app_secret'            => config('oauth.facebook.secret'),
            'default_graph_version' => config('oauth.facebook.default_graph_version')
        ]);

    }

    //로그인
    public function login($datas = [])
    {

        //로그인 인증 성공
        if($this->attemptOAuthLogin($datas)){

            $this->setUserSession(Auth::user());

            session()->regenerate();

            return true;
        }

        return false;
    }


    //회원가입
    public function register($datas = [])
    {
        $user = User::create([
            'email' => $datas['id'],
            'password' => bcrypt($datas['id']),
            'name' => $datas['name'],
            'phone' => '',
            'part'  => 'D',
            'group_idx' => 0,
            'oauth_type' => self::OAUTH_TYPE,
        ]);

        UserProfile::create([
            'user_idx' => $user['idx'],
            'img' => $datas['rofile_image']
        ]);


        Auth::login($user,true);

        $this->setUserSession($user);

        return $user;
    }


    //인증 체크
    public function authorizationCheck($datas = [])
    {
        $this->fb->setDefaultAccessToken($this->acceccToken);

        $response = $this->call('/me','GET',['fields'=>'id,name'])->getGraphUser();
        $response['rofile_image'] =  'http://graph.facebook.com/'.$response['id'].'/picture?width=250&height=250';

        $user = User::where('email', $response['id'])->where('oauth_type', self::OAUTH_TYPE)->get();

        if($user->count() > 0){

            if( $user[0]->oauth_type == self::OAUTH_TYPE)
            {
                $this->login($response);
                return $response;
            }
            //중복 아이디
            return false;

        }else{
            $this->register($response);
        }
        return $response;
    }


    //API 호출
    public function call($api, $method, $datas = [])
    {

        if(strpos($api, '?') === false && !empty($datas)){
            $param = '';
            foreach($datas as $key => $value){
                $param .= ($param ? '&': '' ).$key.'='.$value;
            }

            $api = $api.'?'.$param;
        }

        $response = $this->fb->getClient()->sendRequest($this->fb->request($method,$api));

        //$response = $this->fb->get($api);

        return $response;

    }

    //토큰 저장
    public function setAccessToken($accessToken)
    {
        $this->acceccToken = $accessToken;
    }


    // 인증 시도
    public function attemptOAuthLogin($datas = [])
    {
        return Auth::guard()->attempt([
            'email'=> $datas['id'], 'password'=>$datas['id'], 'oauth_type' => self::OAUTH_TYPE
        ],true);
    }

}