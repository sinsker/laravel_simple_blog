<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-30
 * Time: 오후 4:57
 */

namespace App\Service\OAuth;


class OAuthServiceFactory
{
    const KAKAO = 'kakao';

    const FACEBOOK = 'facebook';

    const NAVER = 'naver';

    const GOOGLE = 'google';


    public static function get($type)
    {
        $service = null;

        switch ($type)
        {
            case self::KAKAO:
                $service = new KakaoOAuthService();
                break;

            case self::FACEBOOK:
                $service = new FacebookOAuthService();
                break;

            case self::NAVER:
                break;

            case self::GOOGLE:
                break;
        }

        return $service;
    }

}