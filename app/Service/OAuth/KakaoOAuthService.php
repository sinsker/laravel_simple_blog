<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-30
 * Time: 오후 4:41
 */

namespace App\Service\OAuth;


use App\Model\User;
use App\Model\UserProfile;
use App\Module\UserSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KakaoOAuthService implements OAuthService
{

    use UserSession;

    private $acceccToken = null;


    const OAUTH_TYPE = 'kakao';

    const HOST = 'https://kapi.kakao.com';

    const API_USER    = '/v1/user/me';

    const API_PROFILE = '/v1/api/talk/profile';


    public function __construct(Request $request = null)
    {

    }

    //로그인
    public function login($datas = [])
    {
        //로그인 인증 성공
        if($this->attemptOAuthLogin($datas)){

            $this->setUserSession(Auth::user());

            session()->regenerate();

            return true;
        }

        return false;
    }


    //회원가입
    public function register($datas = [])
    {
        $user = User::create([
            'email' => $datas['id'],
            'password' => bcrypt($datas['id']),
            'name' => $datas['properties']['nickname'],
            'phone' => '',
            'part'  => 'D',
            'group_idx' => 0,
            'oauth_type' => self::OAUTH_TYPE,
        ]);

        UserProfile::create([
            'user_idx' => $user['idx'],
            'img' => $datas['properties']['profile_image']
        ]);


        Auth::login($user,true);

        $this->setUserSession($user);

        return $user;
    }


    //인증 체크
    public function authorizationCheck($datas = [])
    {
        $response = $this->call(self::API_USER,'GET',$datas);

        $response = \json_decode($response,true);

        //$response = ["kaccount_email"=>"zxzxzczv@naver.com","kaccount_email_verified"=>true,"id"=>395105470,"properties"=>["profile_image"=>"http://mud-kage.kakao.co.kr/14/dn/btqfMo6u3nA/KCGD4dvnEuVWfsiJw0vXik/o.jpg","nickname"=>"이신일","thumbnail_image"=>"http://mud-kage.kakao.co.kr/14/dn/btqfLGzGpXa/MRwH4s24aV7eM08q4Oohnk/o.jpg"]];

        $user = User::where('email', $response['id'])->get();

        if($user->count() > 0){

            if( $user[0]->oauth_type == self::OAUTH_TYPE)
            {
                $this->login($response);

                return $response;
            }

            return false;

        }else{

            $this->register($response);

        }

        return $response;
    }


    //API 호출
    public function call($api, $method, $datas = [])
    {

        if ( is_array($datas)) { // except for uploading
            $params = http_build_query($datas);
        }

        $requestUrl = self::HOST . $api;

        if (($method == 'GET' || $method == 'DELETE' ) && !empty($params)) {
            $requestUrl .= '?'.$params;
        }

        $opts = array(
            CURLOPT_URL => $requestUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSLVERSION => 1,
        );


        if($this->acceccToken == '')
        {
            throw new \Exception('Null Is AcceccToken');
        }

        $headers = array('Authorization: Bearer ' . $this->acceccToken);

        $opts[CURLOPT_HEADER] = false;
        $opts[CURLOPT_HTTPHEADER] = $headers;


        if ($method == 'POST') {
            $opts[CURLOPT_POST] = true;
            if ($params) {
                $opts[CURLOPT_POSTFIELDS] = $params;
            }
        } else if ($method == 'DELETE') {
            $opts[CURLOPT_CUSTOMREQUEST] = 'DELETE';
        }

        $curl_session = curl_init();
        curl_setopt_array($curl_session, $opts);
        $return_data = curl_exec($curl_session);

        if (curl_errno($curl_session)) {
            throw new \Exception(curl_error($curl_session));
        } else {
            // 디버깅 시에 주석을 풀고 응답 내용 확인할 때
            //print_r(curl_getinfo($curl_session));
            curl_close($curl_session);
            return $return_data;
        }

    }

    //토큰 저장
    public function setAccessToken($accessToken)
    {
        $this->acceccToken = $accessToken;
    }


    // 인증 시도
    public function attemptOAuthLogin($datas = [])
    {
        return Auth::guard()->attempt([
            'email'=> $datas['id'], 'password'=>$datas['id'], 'oauth_type' => self::OAUTH_TYPE
        ],true);
    }

}