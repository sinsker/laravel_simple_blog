<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-24
 * Time: 오전 9:22
 */

namespace App\Service;


interface LatestableInterface
{

    public function latestItems($row = 5, array $option = []);

}