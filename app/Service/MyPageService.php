<?php

namespace App\Service;

use App\Model\Board;
use App\Model\Reply;
use App\Model\User;
use App\Model\UserProfile;
use App\Service\CURDInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MyPageService
{

    //유저 정보 가져오기
    public function userInfo($idx)
    {
        $user = User::find($idx);

        $userProfile = $user->userProfile();

        if($userProfile->get()->count() > 0)
        {
            $profile = $userProfile->get()[0];
            $profile->info = nl2br($profile->info);
        }else
        {
            $profile = '';
        }

        return [$user->get()[0], $profile];
    }

    //유저 업데이트
    public function userUpdate($datas)
    {
        $user = User::find(Auth::id());
        $user->phone  = str_replace(['-'],[''],$datas['phone']);
        $user->name = $datas['name'];
        $user->save();

        UserProfile::updateOrCreate(
            ['user_idx' => Auth::id()],
            ['birth' => isset($datas['birth']) && $datas['birth'] != '' ? $datas['birth'] : NULL,
             'name' => $datas['name'],
             'info'  => isset($datas['info']) ? $datas['info'] : '' ]
        );


        return $user;
    }

}