<?php
/**
 * Created by PhpStorm.
 * User: SIEE
 * Date: 2017-02-20
 * Time: 오후 11:13
 */

namespace App\Service;


interface CURDInterface
{

    public function createItem(array $datas);

    public function updateItem($itemKey, array $datas);

    public function readItem($itemKey);

    public function deleteItem($itemKey);
}