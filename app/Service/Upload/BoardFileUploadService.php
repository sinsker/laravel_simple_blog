<?php

namespace App\Service\Upload;

use App\Model\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;

class BoardFileUploadService implements FileUpload
{

    private $uploader;
	
    private $isConnectTable = false;
    
    private $connectTable;
    
    private $connectTableIdx;

    public function __construct(FileUpload $uploader)
    {
        $this->uploader = $uploader;
    }

    //파일 저장
    public function set($key, $rules = '', \Closure $callback = null)
    {

    	if($this->isConnectTable === false) return -1; 
    	
        $this->uploader->setBasePath('/drive/file/'.date('Y/m'));
        
        $fileName = $this->uploader->set($key,$rules);

        $file = Input::file($key);

        if($fileName)
        {
            $this->write([
                'table'     => $this->connectTable,
                'table_idx' => $this->connectTableIdx,
                'path'      => $this->uploader->getSavePath(),
                'name'      => $fileName,
                'oriname'   => $file->getClientOriginalName(),
                'type'      => $file->getClientOriginalExtension(),
                'size'      => $file->getClientSize()
            ], true);
        }

        if($callback !== null) $result = $callback($file);

        return $fileName;
    }
    
    //파일 가져오기
    public function get(array $option = [])
    {
        if(empty($option['table']) || empty($option['idx'])) return false;

		return DB::table('file')->where('table',$option['table'])->where('table_idx',$option['idx'])->get();
    }
    
    //파일 매핑시킬 테이블과 테이블 인덱스 저장
    public function connect($table, $tableIdx){
    	
    	$this->connectTable = $table;
    	
    	$this->connectTableIdx = $tableIdx;
    	
    	$this->isConnectTable = true;
    	
    	return $this;
    }
    
    //db에 저장
    public function write(array $datas, $single = false)
    {

        //테이블의 아이디에 1:1 매칭시
        if($single)
        {
            File::where('table',$datas['table'])->where('table_idx',$datas['table_idx'])->delete();
        }

        return File::create([
            'table'     =>$datas['table'],
            'table_idx' =>$datas['table_idx'],
            'path'      =>$datas['path'],
            'name'      =>$datas['name'],
            'oriname'   =>$datas['oriname'],
            'type'      =>$datas['type'],
            'size'      =>$datas['size'],
            'reg_date'  =>date('Y-m-d H:i:s')
        ]);
    }

    public function drop($key)
    {
        File::destroy($key);
    }




}