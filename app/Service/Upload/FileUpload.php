<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017-03-20
 * Time: 오후 4:56
 */

namespace App\Service\Upload;


interface FileUpload
{


    public function set($key, $rules = '', \Closure $callback = null);

    public function get(array $option = []);

}