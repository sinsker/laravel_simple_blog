<?php

namespace App\Service\Upload;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class FileUploadService implements FileUpload
{

    protected $basePath = 'drive';

    //md5, time, origin
    protected $fileNameType = 'time';


    protected $uploadedFilePath;

    //파일 저장
    public function set($key, $rules = '', \Closure $callback = null)
    {

        $file = Input::file($key);

        if(!empty($rules))
        {

            $ru = array($key =>$file);

            $validator = Validator::make($ru, [$key => $rules ]);

            if ($validator->fails())
            {
                $validator->vaildate();
            }
        }

        if ($file->isValid())
        {
            $OriginalName   = $file->getClientOriginalName();




            if($callback !== null)
            {
                $fileName = $callback($file);

                if ($fileName === false) {
                    return false;
                }

            }else{

                $fileName = $this->fileNameCreate($OriginalName).'.'.$file->getClientOriginalExtension();

            }

            $file->move(public_path().'/'.$this->basePath, $fileName); // uploading file to given path

            $this->uploadedFilePath = public_path().'/'.$this->basePath.'/'.$fileName;

            $this->uploadedFilePath = str_replace('//','/',$this->uploadedFilePath);

            return $fileName;
        }

        return false;
    }


    public function get(array $option = [])
    {

    }

    public function delete(array $option = [])
    {
        if(is_null($option['fileName']) || $option['fileName'] == '') return false;

        $file = public_path().$this->basePath.'/'.$option['fileName'];

        if(is_file($file) == true) {

            unlink($file);

            return true;
        }

        return false;
    }

    //파일 이름 저장 방식 변경
    public function updateFileNameType($type)
    {
        $this->fileNameType = $type;

        return $this;
    }

    //파일 이름 생성
    public function fileNameCreate($fileName)
    {
        switch ($this->fileNameType)
        {
            case 'time':
                return time();
                break;

            case 'md5':
                return md5($fileName);
                break;

            case 'origin':
            defalut:
                return $fileName;
        }

        return $fileName;
    }

    //업로드 완료된 파일 경로 추출
    public function getUploadedFile()
    {
        if(empty($this->uploadedFilePath)) return false;

        return $this->uploadedFilePath;
    }


    //경로 불러오기
    public function getSavePath()
    {
        return $this->basePath;
    }

    //경로 저장하기
    public function setBasePath($path)
    {

        $path = str_replace('.', '/', $path);

        $this->basePath = $path;

        return $this;
    }

}