<?php

namespace App\Service;

use App\Model\Board;
use App\Model\BoardConfig;
use App\Module\Board\PostBoardTool;
use App\Module\Board\TagManager;
use App\Service\CURDInterface;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

//CURD 게시판 서비스단
class BoardService implements CURDInterface, LatestableInterface
{

    use PostBoardTool, TagManager;

    protected $boardModel;

    protected $boardConfigModel;

    public function __construct(Board $boardModel, BoardConfig $boardConfigModel)
    {
        $this->boardModel = $boardModel;

        $this->boardConfigModel = $boardConfigModel;
    }

    //리스트 가져오기
    public function getList($request, $count = 15, array $option = array())
    {

        $queryBuilder = DB::table('board');

        if ( $request->input('word'))
        {
            $queryBuilder->where('content', 'like', '%'.$request->input('word').'%');
        }

        if( isset($option['boardConfigIdx']) && $option['boardConfigIdx'] != '')
        {
            $queryBuilder->where('bc_idx', '=', $option['boardConfigIdx']);
        }

        $list = $queryBuilder
                ->leftJoin('user', 'user.idx', '=', 'board.writer_idx')
                ->leftJoin('user_profile','user_profile.user_idx','=', 'user.idx')
                ->orderBy('board.reg_date', 'desc')
                ->select(['board.*','user_profile.img as user_img','user.part',DB::raw('ifnull(user.name,"알수없음") as name'),DB::raw('(select count(idx) from reply where bidx = board.idx) as reply_cnt'),DB::raw('(select count(board_idx) from board_like where board_idx = board.idx) as like_cnt')])
                ->simplePaginate($count);

        return $list;
    }

    //게시판 카테고리 배열로 가져오기
    public function cateList($stringCate, $split = ',')
    {
        $cateArray = array();

        if(strpos($stringCate, $split) !== false)
        {
            $splitCateArr = explode($split, $stringCate);

            foreach($splitCateArr as $key => $cate){
                if(empty($cate)) continue;

                $cateArray[] = $cate;
            }

            return $cateArray;
        }

        $cateArray[] = $stringCate;

        return $cateArray;
    }

    //카테고리 리스트
    public function cateTotalList($bc_idx){

        $cateArray = DB::table('board')
                    ->select(['cate',DB::raw('count(cate) as cate_row')])
                    ->where('bc_idx','=',$bc_idx)
                    ->where('cate','!=','')
                    ->groupBy('cate')->get();
        return $cateArray;
    }


    //최근 데이터 불러오기
    public function latestItems($row = 5, array $option = [])
    {
        return DB::table('board')->where('bc_idx','=',$option['boardConfigIdx'])->orderBy('idx','desc')->limit($row)->get();
    }


    //게시글 가져오기
    public function readItem($idx)
    {
        //$board = Board::find($idx);

        $board = Board::where('idx', $idx)->select(['board.*', DB::raw('(select count(board_idx) from board_like where board_idx = board.idx) as like_cnt'),DB::raw('(select count(idx) from reply where bidx = board.idx) as reply_cnt')])->get()[0];


        if(empty($board)) return false;

        $board->addHitCount($idx);

        if($user = $board->user()->get()->count() > 0) {
            $user = $board->user()->get()[0];
            $board->name = $user['name'];

            $profile = $user->userProfile();

            if ($profile->get()->count() > 0) {
                $board->user_img = $user->userProfile()->get()[0]['img'];
            } else {
                $board->user_img = '';
            }
        }else{
            //$user = new \stdClass();
            $board->name = '알수없음';
        }

        $board->date = $this->dateFormatKo($board->reg_date);

        if(!empty($board->tag)){
            $board->tagList = $this->tagList($board->tag);
        }

        return $board;
    }

    //게시글 수정하기
    public function updateItem($idx, array $datas)
    {
        $board = Board::find($idx);

        if(isset($datas['tag']) && $datas['tag'] != '')
        {
            $datas['idx'] = $idx;
            $this->autoTagDataUpdata($datas);
        }

        $board->title = $datas['title'];
        $board->content = $datas['content'];
        $board->cate = $datas['cate'];
        $board->notice = $datas['notice'] = isset($datas['notice']) ? $datas['notice'] : 0;
        $board->tag     =  isset($datas['tag']) ? $datas['tag'] : '';

        $board->save();

        return $board;
    }

    public function deleteItem($idx)
    {
        $result = Board::destroy($idx);
    }

    public function createItem(array $datas)
    {

       $board = Board::create([
            'pidx' => 0,
            'dept' => 0,
            'cate'=>$datas['cate'],
            'bc_idx'=> $datas['bc_idx'],
            'notice' => isset($datas['notice']) ? $datas['notice'] : 0,
            'tag'    => isset($datas['tag']) ? $datas['tag'] : '',
            'title'  => $datas['title'],
            'content'=> $datas['content'],
            'writer' => Auth::user()['email'],
            'writer_idx' => Auth::id(),
            'hit' => 0
        ]);

        if(isset($datas['tag']) && $datas['tag'] != '')
        {
            $this->autoTagDataUpdata($board);
        }

        return $board;
    }

    public function tagAss($bc_idx = '', $b_idx = '')
    {
        $tagList = $this->tagAssembly($bc_idx, $b_idx);

        return $tagList;
    }

}