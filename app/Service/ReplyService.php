<?php

namespace App\Service;

use App\Model\Board;
use App\Model\Reply;
use App\Module\Board\PostBoardTool;
use App\Service\CURDInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

//게시판 > 리플 서비스단
class ReplyService implements CURDInterface
{
    use PostBoardTool;

    protected $replyModel;


    public function __construct(\App\Model\Reply $replyModel)
    {
        $this->replyModel = $replyModel;
    }

    public function getReplyList($idx)
    {
        $modelList = DB::table('reply')
            ->join('user', 'user.idx', '=', 'reply.writer_idx')
            ->leftJoin('user_profile','user_profile.user_idx','=', 'user.idx')
            ->where('bidx',$idx)
            ->orderBy('reply.reg_date', 'asc')
            ->select(['reply.*','user_profile.img as user_img','user.part','user.name']);

        $list = $modelList->get();

        foreach($list as  $reply)
        {
            $reply->date = $this->dateFormatKo($reply->reg_date);
            $reply->user_img = profile_img($reply->user_img);
            if($reply->writer_idx == Auth::id()){
                $reply->possession = true;
            }
        }

        return $list;
    }

    //업데이트
    public function updateItem($idx, array $datas)
    {
        $board = Reply::find($idx);

        if (empty($board)) {
            return false;
        }

        $board->content = $datas['content'];
        $board->save();

        return $board;
    }
    
    //삭제
    public function deleteItem($idx)
    {
        $result = Reply::destroy($idx);
    }

    //생성
    public function createItem(array $datas)
    {
        Reply::create([
            'bidx' => $datas['bidx'],
            'content' => $datas['content'],
            'writer' => Auth::user()['email'],
            'writer_idx' => Auth::id(),
            'hit' => 0
        ]);

    }

    public function readItem($itemKey)
    {

    }

}