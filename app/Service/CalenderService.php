<?php

namespace App\Service;

use App\Model\Schedule;
use App\Model\BoardConfig;
use App\Service\CURDInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

//켈린더 서비스 기능
class CalenderService implements LatestableInterface
{

    private $week;

    public function __construct()
    {
        $this->week = config('kanaph.week');
    }

    //해당 달에 속한 모든 스케쥴
    function schedules($year, $month = '', $day = '')
    {

        $schedules = DB::table('schedule')->when($day, function ($query) use ($year, $month, $day) {

                        return $query->where(DB::raw('DATE_FORMAT(start,"%Y-%m-%d")'), '=', $year.'-'.$month.'-'.$day);

                    },function ($query) use ($year, $month){

                        return $query->where(DB::raw('DATE_FORMAT(start,"%Y-%m")'), '=', $year.'-'.$month);

                    })
                    ->orderBy('start','asc')
                    ->get();

        foreach($schedules as $key => $item)
        {
            $this->scueduleDataConvert($item);
        }

        return $schedules;

    }

    //다가오는 일정
    function latestItems($row = 5, array $option = [])
    {
        $schedules = DB::table('schedule')
            ->orderBy('start','asc')
            ->where('START','>=', DB::raw('CURRENT_DATE()'))
            ->limit(5)
            ->get();

        foreach($schedules as $key => $item)
        {
            $this->scueduleDataConvert($item);
        }

        return $schedules;
    }


    //하나의 스케쥴 가져오기
    function get($idx)
    {
        $schedule = Schedule::find($idx);

        $this->scueduleDataConvert($schedule);

        return $schedule;
    }

    //스케쥴 추가
    public function add(array $datas)
    {

        return Schedule::create([
            's_title'	=> $datas['s_title'],
            's_content' => $datas['s_content'],
            'start'		=> $datas['start'],
            'end'		=> isset($datas['end']) ? $datas['end'] : $datas['start'] ,
            'option'	=> isset($datas['option']) ? $datas['option'] : ''
        ]);
    }

    //스케쥴 수정
    public function update($idx, array $datas)
    {
        $schedule = Schedule::find($idx);

        if (empty($schedule)) {
            return false;
        }

        $schedule->s_title = $datas['s_title'];
        $schedule->s_content = $datas['s_content'];
        $schedule->start = $datas['cate'];
        $schedule->end = isset($datas['end']) ? $datas['end'] : $datas['start'];
        $schedule->option = $datas['option'];

        $schedule->save();

        return $schedule;
    }

    //스케줄 삭제
    public function delete($idx)
    {
        $result = Schedule::destroy($idx);
    }


    public function scueduleDataConvert(&$datas)
    {
        $datas->start_m  = date('Y-m-d',strtotime($datas->start));
        $datas->start_w  = $this->week[date('w',strtotime($datas->start))];
        $datas->end_m    = date('Y-m-d',strtotime($datas->end));
        $datas->end_w    = $this->week[date('w',strtotime($datas->end))];
    }

}