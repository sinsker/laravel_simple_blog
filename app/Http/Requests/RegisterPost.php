<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterPost extends FormRequest
{


    public function messages()
    {
        return [
            'email.required' => '이메일을 입력해주시기 바랍니다.',
            'email.unique:user' => '이미 등록 된 이메일 입니다.',
            'email.min' => '이메일을 최소 6자 이상 입력해주시기 바랍니다.',
            'email.max' => '이메일을 최소 10자 이상 입력해주시기 바랍니다.',
            'phone.min' => '이메일을 최대 10자 이상 입력해주시기 바랍니다.',
            'phone.max' => '이메일을 최대 11자 이하 입력해주시기 바랍니다.',
            'name.required' => '이름을 입력해주시기 바랍니다.',
            'password.required' => '비밀번호를 입력해주시기 바랍니다.',
            'password.min' => '비밀번호를 최소 6자 이상 입력해주시기 바랍니다.',
            'password.max' => '비밀번호를 최대 20자 이하 입력해주시기 바랍니다.'
        ];
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|min:6|max:255',
            'phone' => 'required|min:10|max:11',
            'name' => 'required|max:255',
            'password' => 'required|min:6|max:20|confirmed'
        ];
    }
}
