<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BbsPost extends FormRequest
{

    public function messages()
    {
        return [
            'title.required' => '제목을 입력해주시기 바랍니다.',
            'content.required' => '내용을 입력해주시기 바랍니다.'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required'
        ];
    }


    //후킹
    /*
    public function withValidator($validator)
    {
       $validator->after(function ($validator) {
            if ($this->somethingElseIsInvalid()) {
                $validator->errors()->add('field', 'Something is wrong with this field!');
            }
        });
    }
*/

}
