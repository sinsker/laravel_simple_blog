<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate;

class BBSAccessUserLevelCheck extends Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {

        $parentHandle = parent::handle($request, $next, $guards);

        $group = session('group',0);

        if($group['level'] == 0)
        {
            return redirect('/')->withInput()->withErrors('해당 유저의 권한이 없습니다.');
        }

        return $parentHandle;
    }

}
