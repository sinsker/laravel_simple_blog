<?php

namespace App\Http\Controllers;

use App\Model\File;
use App\Module\OpenGraphCreator;
use App\Service\Upload\BoardFileUploadService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\BbsPost;

use App\Model\BoardConfig;
use App\Model\Board;
use App\Service\BoardService;
use App\Service\Upload\FileUploadService;

use App\Module\Board\PostBoardTool;
use App\Module\Board\TagManager;
use App\Module\Board\Like\BoardLikeActionProxy;
use Illuminate\Support\Facades\Input;

class BBSController extends Controller
{

    use PostBoardTool, TagManager;

    /*use DataTypeConverter; //데이터 형태 변경*/

    protected $boardService;

    protected $fileService;

    protected $boardLikeAction;

    private $config = [];



    //공통사항 카테고리,최신글,
    public function __construct(BoardService $boardService, FileUploadService $fileService, BoardLikeActionProxy $boardLikeAction)
    {

        $this->config = config('bbs');

        $this->boardService = $boardService;

        $this->fileService = $fileService;

        $this->boardLikeAction = $boardLikeAction;

    }

    //게시판 목록 View
    public function index(Request $request, $cidx)
    {

        if( $cidx == 1 && Auth::check() == false){
            session(['referer' => $_SERVER['REQUEST_URI']]);
            return redirect('/login');
        }

        //게시판 옵션
        $boardConfig = BoardConfig::find($cidx);
        $boardConfig->cate = $this->boardService->cateList($boardConfig->bc_cate, ',');

        //게시글
        $boards = $this->boardService->getList($request, $boardConfig->bc_row, ['boardConfigIdx' => $cidx]);

        //좋아요
        $likeKeys = array_keys($this->boardLikeAction->isLikeUser());

        //리스트 전체
        $this->cover($boards,function(&$item) use ($likeKeys) {

            $item->content = $this->shortcutText($item->content, 100);

            if(!empty($item->tag)) $item->tagList = $this->tagList($item->tag);

            if(isset($item->src)) $item->src = preg_replace('/style=(\"|\')?([^\"\']+)(\"|\')?/i','',$item->src);

            if(in_array($item->idx, $likeKeys)) $item->liked = true;

            if(!empty($item->tag)) $item->tagList = $this->tagList($item->tag);

            return $item;

        });

        $viewData = [
            'boards'        => $boards,
            'boardConfig'   => $boardConfig
        ];

        $viewData = array_merge($viewData,$this->publicGroupBoardDatas($cidx));

        return view($this->skin($boardConfig->bc_type).'/list', $viewData);

    }

    //태그 검색 리스트
    public function tag($id)
    {
        $tagList = $this->tagSearch($id);

        //리스트 전체
        $this->cover($tagList,function(&$item){

            $item->content = $this->shortcutText($item->content, 100);

            if(isset($item->src)) $item->src = preg_replace('/style=(\"|\')?([^\"\']+)(\"|\')?/i','',$item->src);

            return $item;

        });

        return view('kanaph/bbs/tag',['list'=>$tagList,'id'=>$id]);
    }

    //게시판 단일 View
    public function show($cidx, $idx)
    {
        if( $cidx == 1 && Auth::check() == false){
            session(['referer' => $_SERVER['REQUEST_URI']]);
            return redirect('/login');
        }

        $boardConfig = BoardConfig::find($cidx);

        //좋아요
        if(!$board = $this->boardService->readItem($idx)){
            return redirect()->route('bbs.list',['cidx'=>$boardConfig->idx])->withErrors('게시판이 존재하지 않습니다.')->withInput();
        };

        $likeKey = key($this->boardLikeAction->isLikeUser(['board_idx'=> $board->idx]));

        $board->idx == $likeKey ? $board->liked = true : '';

        $files = (new BoardFileUploadService($this->fileService))->get(['table'=>'board', 'idx'=>$board->idx]);

        //$this->cover($board);

        $viewData = [
            'board'         => $board,
            'boardConfig'   => $boardConfig,
            'files'         => $files,
            'og'            => OpenGraphCreator::init()->title($board->title)->description($board->content)
        ];

        $viewData = array_merge($viewData, $this->publicGroupBoardDatas($cidx));


        return view($this->skin($boardConfig->bc_type).'/view',$viewData);
    }

    //게시물 등록 View
    public function create($cidx)
    {

        $boardConfig = BoardConfig::find($cidx);

        $boardConfig->cate = $this->boardService->cateList($boardConfig->bc_cate, ',');

        return view($this->skin($boardConfig->bc_type).'/edit')->with('boardConfig', $boardConfig);

    }

    //게시판 수정 View
    public function edit($cidx, $idx)
    {
        if(!$board = $this->boardService->readItem($idx))
        {
            return redirect()->route('bbs.list')->withErrors('게시판이 존재하지 않습니다.')->withInput();
        }

        if($board->writer_idx != Auth::id())
        {
            return redirect()->route('bbs.list')->withErrors('본인 소유의 게시글이 아닙니다.')->withInput();
        }

        $boardConfig = BoardConfig::find($cidx);

        $boardConfig->cate = $this->boardService->cateList($boardConfig->bc_cate, ',');

        $files = (new BoardFileUploadService($this->fileService))->get(['table'=>'board', 'idx'=>$board->idx]);


        $viewData = [
            'board'         => $board,
            'boardConfig'   => $boardConfig,
            'files'         => $files
        ];

        $viewData = array_merge($viewData, $this->publicGroupBoardDatas($cidx));

        return view($this->skin($boardConfig->bc_type).'/edit',$viewData);
    }

    //게시판 수정
    public function update(BbsPost $request, $idx)
    {
        $board = $this->boardService->updateItem($idx,$request->all());

        if($board && $request->hasFile('file'))
        {
        	$file = (new BoardFileUploadService($this->fileService))->connect('board',$board->idx)->set('file');
        }
        
        if(!$board){
            redirect()->route('bbs.list');
        };

        return  redirect()->route('bbs.view',['idx'=>$board->idx,'cidx'=>$board->bc_idx]);
    }

    //게시판 삭제
    public function destroy($idx)
    {
        $board = Board::find($idx);

        if($board->writer_idx != Auth::id()){
            return redirect()->route('bbs.list',['cidx'=>$board->bc_idx])->withErrors('본인 소유의 게시글이 아닙니다.')->withInput();
        }

        $result = $this->boardService->deleteItem($idx);

        if($board && $result)
        {
            $file = (new BoardFileUploadService($this->fileService))->get(['table'=>'board','idx'=>$board->idx]);

            if(\Illuminate\Support\Facades\File::exists($filePath = public_path().'/'.$file->path.'/'.$file->name))
            {
                \Illuminate\Support\Facades\File::delete($filePath);
            }

            File::destroy($file->idx);
        }

        return redirect()->route('bbs.list',['cidx'=>$board->bc_idx]);
    }

    //게시물 등록
    public function store(BbsPost $request)
    {

        $board = $this->boardService->createItem($request->all());
        
       	if($board && $request->hasFile('file'))
       	{
       		$file = (new BoardFileUploadService($this->fileService))->connect('board',$board->idx)->set('file');
       	}

        return redirect()->route('bbs.view',['idx'=>$board->idx,'cidx'=>$board->bc_idx]);
    }


    //공통 메뉴
    public function publicGroupBoardDatas($boardConfigIdx)
    {
        //태그
        $tags = $this->boardService->tagAss($boardConfigIdx);

        //최신 등록글
        $latestItems = $this->boardService->latestItems(3,['boardConfigIdx'=>$boardConfigIdx]);

        //최근 등록된 리스트
        $this->cover($latestItems, function(&$item){


            $item->title =  $this->htmlTagFormat($item->title, ['p','strong','span'],'');

            $item->content =  $this->htmlTagFormat($item->content, ['p','strong','span'],'');

            $item->content = $this->shortcutText($item->content, 25);

            if(isset($item->src)) $item->src = preg_replace('/style=(\"|\')?([^\"\']+)(\"|\')?/i','class="pull-left" style="width:65px"',$item->src);

            return $item;
        });

        //카테고리 분류
        $categories = $this->boardService->cateTotalList($boardConfigIdx);


        return ['categories'    => $categories,
                'latestItems'   => $latestItems,
                'tags'          => $tags];
    }


    //==================================================================================================================================================//

    //게시판 좋아요 개수
    public function likeCount($idx)
    {
        return response()->json(['count'=> $this->boardLikeAction->boardLikeCount(['board_idx' => $idx])]);
    }

    //게시판 좋아요 등록
    public function likeRegister($idx)
    {
        if(count($this->boardLikeAction->isLikeUser(['board_idx' => $idx])) > 0 ) return response()->json(['like'=>false]);

        return response()->json(['like'=> $this->boardLikeAction->boardLike(['board_idx' => $idx])]);
    }

    //게시판 좋아요 해제
    public function likeCancel($idx)
    {
        if(count($this->boardLikeAction->isLikeUser(['board_idx' => $idx])) <= 0 ) return response()->json(['unlike'=>false]);

        $this->boardLikeAction->boardUnlike(['board_idx' => $idx]);

        return response()->json(['unlike'=>true]);
    }

    //==================================================================================================================================================//

}
