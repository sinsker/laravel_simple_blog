<?php

namespace App\Http\Controllers;

use App\Module\DataTypeConverter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Service\CalenderService as Service;

class CalendarController extends Controller
{

    use DataTypeConverter;

    protected $service;


    public function __construct(Service $service)
    {
        $this->service = $service;

        $lastest =  $this->service->latestItems();

        session(['referer' => $_SERVER['REQUEST_URI']]);

    }

    //캘린더 List
    public function calendar($year = '', $month = '', $day = '')
    {

        if(empty($year) && empty($month) && empty($day) )
        {
            $year   = date('Y');
            $month  = date('m');
        }

        $datas = $this->service->schedules($year, $month, $day);

        /*return view('kanaph/calendar',['schedules',$datas]);*/

        return $this->returnView(['schedules' => $datas],'kanaph/calendar');
    }

    //캘린더 가져오기
    public function get($idx)
    {
        $schedule = $this->service->get($idx);

        return $this->returnView(['schedule'=>$schedule],'kanaph/calendar');
    }

    //게시판 수정
    public function update(Request $request, $idx)
    {
        $calendar = $this->service->update($idx, $request->all());

        return redirect()->route('kanaph.calendar');
    }

    //게시판 삭제
    public function destroy($idx)
    {
        $this->service->delete($idx);

        return redirect()->route('kanaph.calendar');
    }

    //캘린더 추가
    public function add(Request $request)
    {
        $calendar = $this->service->add($request->all());

        return redirect()->route('kanaph.calendar');
    }
}
