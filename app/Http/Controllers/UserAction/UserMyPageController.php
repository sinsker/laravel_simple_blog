<?php
/**
 * Created by PhpStorm.
 * User: SIEE
 * Date: 2017-03-16
 * Time: 오후 9:52
 */

namespace App\Http\Controllers\UserAction;


use App\Events\UserImageUploadEvent;
use App\Http\Controllers\Controller;
use App\Model\UserProfile;
use App\Module\UserSession;
use App\Service\MyPageService;
use App\Service\Upload\FileUploadService;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Http\Request;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;

class UserMyPageController extends Controller
{
    use UserSession;


    private $fileUploadService, $myPageService;


    public function __construct(FileUploadService $fileUploadService, MyPageService $myPageService)
    {
        session(['referer' => $_SERVER['REQUEST_URI']]);

        $this->middleware('auth');

        $this->fileUploadService = $fileUploadService;

        $this->myPageService = $myPageService;

    }

    //유저 메인화면
    public function index()
    {
        list($user, $userProfile) = $this->myPageService->userInfo(Auth::id());

        return view('kanaph/mypage/info',['user'=>$user, 'profile'=>$userProfile]);
    }

    //유저 수정화면
    public function edit()
    {
        list($user, $userProfile) = $this->myPageService->userInfo(Auth::id());

        return view('kanaph/mypage/info_edit',['user'=>$user, 'profile'=>$userProfile]);
    }

    //유저 정보 업데이트
    public function update(Request $request)
    {
        $user = $this->myPageService->userUpdate($request->all());

        $this->setUserSession($user);

        return redirect()->route('mypage.index');
    }


    //유저 이미지 변경
    public function userImgUpdate(Request $request)
    {

        $this->fileUploadService->updateFileNameType('md5')->setBasePath('drive/profile/'.Auth::user()['name']);

        $this->fileUploadService->set('img','mimes:jpeg,bmp,png,jpg|required|max:3000',function($file){

            UserProfile::updateOrCreate(
                ['user_idx' => Auth::id()],
                ['img'   => $this->fileUploadService->getSavePath().'/'.$this->fileUploadService->fileNameCreate(Auth::user()['name']).'.'.$file->getClientOriginalExtension()]
            );

            return $this->fileUploadService->fileNameCreate(Auth::user()['name']).'.'.$file->getClientOriginalExtension();
        });

        event(new UserImageUploadEvent($this->fileUploadService->getUploadedFile()));

        $this->setUserSession();

    }

}