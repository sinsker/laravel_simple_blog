<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ReplyController extends Controller
{

    private $replyService;

    //리플서비스 객체 DI
    public function __construct(\App\Service\ReplyService $replyService)
    {
        $this->replyService = $replyService;
    }

    //리플 리스트
    public function list($bidx)
    {
        $replys = $this->replyService->getReplyList($bidx);

        return response()->json(['replys'=>$replys]);
    }

    //리플 등록
    public function store(Request $request)
    {
        $this->replyService->createItem($request->all());

        return redirect()->route('bbs.view',['bc_idx'=> Input::get('bc_idx'),'idx'=>Input::get('bidx')]);
    }

    //리플 업데이트
    public function update(Request $request)
    {
        $requestData = $request->all();

        $this->replyService->updateItem($request->input('idx'), $requestData);

        return redirect()->route('bbs.view',['bc_idx'=> Input::get('bc_idx'),'idx'=>Input::get('bidx')]);
    }

    //리플 삭제
    public function delete(Request $request)
    {
        $this->replyService->deleteItem(Input::get('idx'));

        return redirect()->route('bbs.view',['bc_idx'=> Input::get('bc_idx'),'idx'=>Input::get('bidx')]);
    }

}

