<?php

namespace App\Http\Controllers\File;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Service\Upload\FileUploadService;

use App\Http\Controllers\Controller;

class FileUploaderController extends Controller
{

    private $fileUploadService;

    public function __construct(FileUploadService $fileUploadService)
    {
        $this->fileUploadService = $fileUploadService;

    }

    //보드글 이미지 업로드
    public function boardImageUpload(Request $request)
    {
        $this->fileUploadService->setBasePath('/drive/board/'.date('Y/m'));

        $files = Input::file();

        $return = [];

        foreach($files as $key => $file)
        {
            $return[$key] = $this->fileUploadService->set($key);
        }

        return response()->json($return);
    }


}
