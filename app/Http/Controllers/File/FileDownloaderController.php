<?php

namespace App\Http\Controllers\File;

use App\Service\Upload\BoardFileUploadService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use App\Service\Upload\FileUploadService;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class FileDownloaderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }


    //파일 다운로드
    public function singleDownload($idx)
    {

       $files = DB::table('file')->where('idx',$idx)->get();

       if($files->count() > 0)
       {
           $file = $files[0];
           
           return Response::download(public_path().'/'.$file->path.'/'.$file->name, $file->oriname);
       }

       return false;
    }

    //파일 삭제
    public function delete($idx)
    {

        $files = DB::table('file')->where('idx',$idx)->get();

        if($files->count() > 0)
        {
            $file = $files[0];

            if(File::exists($filePath = public_path().'/'.$file->path.'/'.$file->name))
            {
                File::delete($filePath);
            }

            DB::table('file')->where('idx',$idx)->delete();
        }

    }

}
