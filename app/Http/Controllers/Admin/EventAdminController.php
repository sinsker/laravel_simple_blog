<?php

namespace App\Http\Controllers\Admin;

use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Model\BoardConfig;
use App\Http\Controllers\Controller;

class EventAdminController extends Controller
{

    //관리자 페이지
    public function index($idx = '')
    {
        $vars = config('bbs');

        $configList = $this->configList('board_config');

        $data = [
            'config' => $vars,
            'configList' => $configList,
        ];

        if(!empty($idx))
        {
            $view = BoardConfig::find($idx);

            $data['view'] = $view;
        }

        return view('kanaph/admin/index',$data);
    }

    //게시판 수정
    public function edit($idx, array $datas)
    {
        $boardConfig = BoardConfig::find($idx);

        if (empty($boardConfig)) {
            return false;
        }

        $boardConfig->bc_name = $datas['bc_name'];
        $boardConfig->bc_cate = $datas['bc_cate'];
        $boardConfig->bc_row  = $datas['bc_row'];
        $boardConfig->bc_type = $datas['bc_type'];
        $boardConfig->bc_info = $datas['bc_info'];

        $boardConfig->save();

        return redirect()->route('admin.kanaph');
    }

    //저장
    public function create($data)
    {
        BoardConfig::create([
            'bc_name'=> $data['bc_name'],
            'bc_row' => $data['bc_row'],
            'bc_cate' => $data['bc_cate'],
            'bc_info' => $data['bc_info'],
            'bc_type' => $data['bc_type'],
        ]);

        return redirect()->route('admin.kanaph');
    }

    //cofig save
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        return $this->create($request->all());

    }

    //config update
    public function update($idx, Request $request)
    {
        return $this->edit($idx, $request->all());
    }

    //저장시 유효성 검사
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'bc_name' => 'required|min:3|max:255|unique:board_config'
        ]);
    }


    //유저 정보 가져오기
    public function member($idx = '')
    {
        $vars = config('kanaph');

        $configList = $this->configList('user');

        $data = [
            'config' => $vars,
            'configList' => $configList,
        ];

        if(!empty($idx))
        {
            $view = User::find($idx);

            if($view->userProfile()->get()->count() > 0)
            {
                $profile = $view->userProfile()->get()[0];
                $data['profile'] = $profile;
            }

            $data['user'] = $view;
        }

        return view('kanaph/admin/members',$data);
    }

    public function memberUpdate($idx, Request $request)
    {
        $user = User::find($idx);

        if (empty($user)) {
            return false;
        }

        $user->part = $request->input('part');
        $user->group_idx = $request->input('group');
        $user->phone  = $request->input('phone');
        $user->name = $request->input('name');

        $user->save();

        return redirect()->route('admin.kanaph.member');
    }
    
    //config list
    public function configList($board)
    {
    	return DB::table($board)->select(['*'])->paginate(10);
    }
    
}

