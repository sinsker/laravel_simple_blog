<?php

namespace App\Http\Controllers\Auth;

use App\Model\User;
use App\Module\UserSession;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    use UserSession;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
*/
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'email' => 'required|min:6|max:255|unique:user',
            'phone' => 'required|unique:user',
            'name' => 'required|max:255',
            'password' => 'required|min:6|max:20|confirmed'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
       $user = User::where('email', $data['email'])->orWhere('phone', $data['phone'])->get();


        if(!empty($user[0]->email)) return $user;

        return User::create([
            'email' => $data['email'],
            'phone' => $data['phone'],
            'name'  => $data['name'],
            'part'  => 'D',
            'group_idx' => 0,
            'password'  => bcrypt($data['password']),
        ]);
    }

    public function showRegistrationForm()
    {
        return view('kanaph.auth.register');
    }

    public function registered(Request $request, $user)
    {
        $this->setUserSession($user);

        $referer = session('referer',$this->redirectPath());

        return redirect($referer);
    }
}
