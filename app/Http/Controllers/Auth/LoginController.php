<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Module\OpenGraphCreator;
use App\Module\UserSession;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    use ValidatesRequests;

    use UserSession;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    

    public function username()
    {
        return 'email';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout','remember']]);
    }
    

   /* public function authenticate()
    {
        if (Auth::attempt(['phone' => $phone, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
    }*/

    public function showLoginForm()
    {
        return view('kanaph.auth.login',['og'=> OpenGraphCreator::init()->title('[KANAPH] 문화사역팀입니다.')->description('CCD,연극,미디어 매체를 통해 복음을 전파하는 문화사역팀입니다.')]);
    }


    protected function authenticated(Request $request, $user)
    {
        $this->setUserSession($user);

        $referer = session('referer',$this->redirectPath());

        return redirect($referer);
    }

}
