<?php

namespace App\Http\Controllers\Auth\OAuth;

use App\Http\Controllers\Controller;
use App\Service\OAuth\OAuthService;
use App\Service\OAuth\OAuthServiceFactory;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OAuthController extends Controller/**/
{

    /*use AuthenticatesUsers;

    use ValidatesRequests;*/

    private $oauthService;


    public function __construct()
    {
        //$this->middleware('guest', ['except' => ['logout','remember']]);
    }


    //로그인 접근
    public function index(Request $request)
    {
        $params = $request->all();

        $oauthService = OAuthServiceFactory::get($params['type']);

        $oauthService->setAccessToken($params['access_token']);

        return $this->authentication($oauthService, $params);
    }


    //OAUTH 인증 밎 로그인
    public function authentication(OAuthService $oauthService, $params = [])
    {

        $response = $oauthService->authorizationCheck($params);

        //중복로그인
        if($response === false) $return = ['result' => false, 'massage'=> '중복된 아이디로 인하여 로그인에 실패하였습니다.'];

        if(Auth::check()) {
            $return = ['result' => true, 'referer' => session('referer','/'), 'info'=>[$response]];
        }else{
            $return = ['result' => false, 'massage'=>'인증에 실패하였습니다.', 'info'=>[$response]];
        }
        return $return;
    }

    //API 호출
    public function callAPI()
    {

    }


}
