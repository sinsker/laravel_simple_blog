<?php

namespace App\Http\Controllers;

use App\Module\Board\Like\BoardLikeActionProxy;
use App\Module\Board\PostBoardTool;
use App\Module\Board\TagManager;
use App\Module\OpenGraphCreator;
use App\Service\BoardService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{

    use PostBoardTool, TagManager;

    protected $boardService;
    protected $boardLikeAction;

    private $config = [];

    function __construct(BoardService $boardService, BoardLikeActionProxy $boardLikeAction)
    {
        session(['referer' => $_SERVER['REQUEST_URI']]);

        $this->config = config('bbs');

        $this->boardService = $boardService;

        $this->boardLikeAction = $boardLikeAction;

    }

    //메인화면
    public function index(Request $request)
    {
        //최신 등록글
        $latestItems = $this->boardService->latestItems(3,['boardConfigIdx'=>2]);

        return view('kanaph/index',['og'=> OpenGraphCreator::init()->title('[KANAPH] 문화사역팀입니다.')->description('CCD,연극,미디어 매체를 통해 복음을 전파하는 문화사역팀입니다.')]);
    }

    //메인화면
    public function main(Request $request)
    {
        //최신 등록글

        return view('kanaph/main',['og'=> OpenGraphCreator::init()->title('[KANAPH] 문화사역팀입니다.')->description('CCD,연극,미디어 매체를 통해 복음을 전파하는 문화사역팀입니다.')]);
    }

    //팀소개
    public function info()
    {
        return view('kanaph/info');
    }

    //맴버소개
    public function members()
    {
        $config = config('kanaph');

        $users = DB::select('select * from user left outer join user_profile on user.idx = user_profile.user_idx where group_idx >= 1');

        foreach($users as $item)
        {
           $groupName = $config['group'][$item->group_idx];
           $part      = $config['part'][$item->part];

           $item->groupName = $groupName;
           $item->part = $part;
        }

        return view('kanaph/info_member',['users'=>$users]);
    }

    public function movie()
    {
        //https://www.youtube.com/channel/UCI2eFOdtZzV00YT9Y-27T7w
        return view('kanaph/movie');
    }
}
