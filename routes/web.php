<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//기본 홈
Route::get('/', 'MainController@index')->name('main');
Route::get('/main', 'MainController@main')->name('main2');

//회원가입&로그인
Auth::routes();

Route::post('/login/oauth','Auth\OAuth\OAuthController@index')->name('login.oauth');

/*Route::get('remember', 'Auth\LoginController@rememberForm')->name('auth.remember');
Route::post('remember', 'Auth\LoginController@remember')->name('auth.remember.registar');*/

//토큰 받기
Route::post('jwt/token', 'Auth\APIController@token')->name('jwt.login');
Route::get('jwt/token/{token}', 'Auth\APIController@user')->name('jwt.token');

//CURD 게시판
/*Route::Resource('board', 'BoardController');*/

//게시판 > Reply 기능
Route::get('/reply/{bidx}', 'ReplyController@list')->name('reply.list')->middleware('auth');
Route::post('/reply', 'ReplyController@store')->name('reply.store')->middleware('auth');
Route::put('/reply', 'ReplyController@update')->name('reply.update')->middleware('auth');
Route::delete('/reply', 'ReplyController@delete')->name('reply.delete')->middleware('auth');


//Kanph 페이지
Route::get('/','MainController@index')->name('kanaph.index');
Route::get('info','MainController@info')->name('kanaph.info');
Route::get('info/member','MainController@members')->name('kanaph.info.member');
Route::get('movie','MainController@movie')->name('kanaph.movie');


//Calendar
Route::get('calendar','CalendarController@calendar')->name('kanaph.calendar');
Route::get('calendar/{year}/{month}','CalendarController@calendar')->name('kanaph.calendar.list');
Route::get('calendar/{year}/{month}/{day}','CalendarController@calendar')->name('kanaph.calendar.day');
Route::get('calendar/{idx}','CalendarController@get')->name('kanaph.calendar.get');
Route::post('calendar','CalendarController@add')->name('kanaph.calendar.add');
Route::put('calendar/{idx}','CalendarController@update')->name('kanaph.calendar.update');
Route::delete('calendar/{idx}','CalendarController@delete')->name('kanaph.calendar.delete');


//BBS CURD
Route::get('post/{cidx}', 'BBSController@index')->name('bbs.list');
Route::get('post/{cidx}/{idx}', 'BBSController@show')->name('bbs.view');

Route::group(['middleware' => 'bbs.level'], function () {
    Route::get('post/{cidx}/create', 'BBSController@create')->name('bbs.create');
    Route::get('post/{cidx}/edit/{idx}', 'BBSController@edit')->name('bbs.edit');
    Route::post('post', 'BBSController@store')->name('bbs.store');
    Route::put('post/{idx}', 'BBSController@update')->name('bbs.update');
    Route::delete('post/{idx}', 'BBSController@destroy')->name('bbs.delete');
});

//BBS Tag
Route::get('tag/{id}', 'BBSController@tag')->name('tag.list');

//BBS 좋아요 기능
Route::group(['middleware' => 'auth'], function () {
    Route::get('like/{idx}', 'BBSController@likeCount')->name('like.count');
    Route::get('like/{idx}/love', 'BBSController@likeRegister')->name('like.register');
    Route::get('like/cancel/{idx}', 'BBSController@likeCancel')->name('like.cancel');
});

//Kanaph 관리자 페이지
Route::group(['middleware' => 'bbs.level'], function () {
    Route::get('kanaph/admin', 'Admin\EventAdminController@index')->name('admin.kanaph');
    Route::get('kanaph/admin/{idx}', 'Admin\EventAdminController@index')->name('admin.kanaph.view');
    Route::post('kanaph/admin', 'Admin\EventAdminController@store')->name('admin.kanaph.store');
    Route::put('kanaph/admin/{idx}', 'Admin\EventAdminController@update')->name('admin.kanaph.update');

    Route::get('kanaph/member', 'Admin\EventAdminController@member')->name('admin.kanaph.member');
    Route::get('kanaph/member/{idx}', 'Admin\EventAdminController@member')->name('admin.kanaph.member.view');
    Route::put('kanaph/member/{idx}', 'Admin\EventAdminController@memberUpdate')->name('admin.kanaph.member.update');
});

//마이 페이지
Route::get('mypage', 'UserAction\UserMyPageController@index')->name('mypage.index');

Route::get('mypage/edit', 'UserAction\UserMyPageController@edit')->name('mypage.edit');
Route::put('mypage', 'UserAction\UserMyPageController@update')->name('mypage.update');

Route::put('mypage/img', 'UserAction\UserMyPageController@userImgUpdate')->name('mypage.img');

//Upload set
Route::post('upload/board/img','File\FileUploaderController@boardImageUpload')->name('upload.board.img');

//Download
Route::get('down/{idx}','File\FileDownloaderController@singleDownload')->name('down.single');
Route::delete('down/{idx}','File\FileDownloaderController@delete')->name('down.delete');

// phpinfo //
/*Route::get('/phpinfo',function(){
    return phpinfo();
});*/
// phpinfo //

