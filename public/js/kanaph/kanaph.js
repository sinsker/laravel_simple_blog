var oauth = {
    'kakao' : '5c4dc52100bb1ea045aed87ea749829a',
    'facebook' : '290203064748942',
    'inster' : ''
};

Kakao.init(oauth.kakao);

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/ko_KR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
    FB.init({
        appId      : oauth.facebook,
        status     : true,
        xfbml      : true,
        cookie     : true,
        version    : 'v2.8'
    });

};


var copyToClipboard = function(str) {
    if (window.clipboardData) {
        window.clipboardData.setData('text', str);
        alert('복사되었습니다.     ');
    } else {
        window.prompt('Ctrl+C 를 누른다음 Enter를 치시면 복사됩니다.', str);
    }
};


function share_kakao(title, subtitle, url, like_cnt, comment_cnt, image) { //http://sync2030.org/sync/resources/images/note0.jpg

    var imageUrl = image ? image : 'http://kanaph.site/images/kanaph/_1hdUd015aufd9sv9fqoh_qg59pa.jpg';

    console.log(like_cnt);
    console.log(comment_cnt);

    Kakao.Link.sendDefault({
        objectType: 'feed',
        content: {
            title: title,
            description: subtitle,
            imageUrl: imageUrl,
            link: {
                mobileWebUrl: url
            }
        },
        social: {
            likeCount: Number(like_cnt),
            commentCount: Number(comment_cnt)
        },
        buttons: [{
            title: '웹으로 이동',
            link: {
                mobileWebUrl: url
            }
        }]
    });
}

function share_facebook(url){
    var sns_popup = window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&t='+encodeURIComponent(url),'facebook_share_dialog','toolbar=0,status=0,width=550,height=436');
    if (sns_popup.focus) {
        sns.popup.focus();
    }
}

function share_url(url){
    if (!window.clipboardData) {
        prompt("Ctrl+C를 눌러 클립보드로 복사하세요.", url);
    }else{
        if(confirm("링크 주소를 클립보드에 복사하시겠습니까?")){
            window.clipboardData.setData("Text", url);
            alert("링크가 복사되었습니다.");
        }
    }
}
