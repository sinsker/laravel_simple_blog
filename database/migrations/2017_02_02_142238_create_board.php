<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board', function (Blueprint $table) {
            $table->increments('idx');
            $table->integer('pidx')->default(0);
            $table->integer('dept')->default(0);
            $table->integer('bc_idx');
            $table->string('cate');
            $table->text('tag')->nullable();
            $table->tinyInteger('notice')->default(0);
            $table->string('title');
            $table->text('content');
            $table->string('writer');
            $table->integer('writer_idx');
            $table->integer('hit')->default(0);
            $table->dateTime('reg_date');
            $table->dateTime('up_date');

            $table->index('bc_idx','board_config_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('board');
    }
}
