<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tag', function (Blueprint $table) {
            $table->string('id');
            $table->string('location');
            $table->integer('bc_idx');
            $table->string('writer');
            $table->integer('writer_idx');

            $table->timestamps();

            $table->primary(['id','location']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tag');
    }
}
