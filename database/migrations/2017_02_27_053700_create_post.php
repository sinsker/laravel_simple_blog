<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post', function (Blueprint $table) {
            $table->increments('idx');
            $table->integer('pidx')->default(0);
            $table->integer('dept')->default(0);
            $table->tinyInteger('new')->default(0);
            $table->tinyInteger('notice')->default(0);
            $table->string('title');
            $table->text('content');
            $table->string('writer');
            $table->integer('writer_idx');
            $table->integer('hit')->default(0);
            $table->dateTime('reg_date');
            $table->dateTime('up_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post');
    }
}
