<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLike extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('board_like', function (Blueprint $table) {
            $table->integer('board_idx');
            $table->integer('user_idx');
            $table->enum('type', ['good','love','sad','angry','shame'])->default('love');

            $table->primary(['board_idx','user_idx']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('board_like');
    }
}
