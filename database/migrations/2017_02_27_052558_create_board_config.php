<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_config', function (Blueprint $table) {
            $table->increments('idx');
            $table->integer('bc_row')->default(15);
            $table->string('bc_name');
            $table->string('bc_cate');
            $table->integer('bc_type')->default(0);
            $table->text('bc_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('board_config');
    }
}
