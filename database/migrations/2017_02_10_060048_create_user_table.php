<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('idx');

            $table->integer('group_idx')->unsigned()->unllable();
            $table->string('part', 20)->default('D');

            $table->enum('oauth_type',['kakao','facebook','naver','google'])->nullable();

            $table->string('email')->unique();
            $table->string('phone')->unique();

            $table->string('name');
            $table->string('password');


            $table->timestamp('access_at')->nullable();

            $table->rememberToken();
            $table->timestamps();

            $table->index('email', 'my_index_email');
            $table->index('phone', 'my_index_phone');

        });



        Schema::create('user_profile', function (Blueprint $table) {

            $table->integer('user_idx')->unsigned();
            $table->primary('user_idx');

            $table->string('img',255)->default('');
            $table->date('birth')->nullable();
            $table->string('job',50)->default('');
            $table->text('info')->nullable();

            $table->foreign('user_idx')->references('idx')->on('user')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_profile');
        Schema::drop('user');
    }
}
