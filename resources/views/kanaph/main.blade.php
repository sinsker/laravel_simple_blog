<!DOCTYPE html>
<html lang="ko">
<head>
    <title>KANAPH : 문화사역팀</title>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="카나프, kanaph, 성락, 성락성결교회, sync, 싱크">
    <meta name="keywords" content="카나프, kanaph, 성락, 성락성결교회, sync, 싱크, ccd, 특송, 워십, 문화사역, 사역">
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="/lib/global/css/main.css">
    {!! $og->template() !!}
</head>
<body>

<!-- notification for small viewports and landscape oriented smartphones -->
{{--<div class="device-notification">
    <a class="device-notification--logo" href="#0">
        <img src="/lib/global/img/logo.png" alt="Global">
        <p>KANAPH</p>
    </a>
    <p class="device-notification--message">Global has so much to offer that we must request you orient your device to portrait or find a larger screen. You won't be disappointed.</p>
</div>--}}

<div class="perspective effect-rotate-left">
    <div class="container"><div class="outer-nav--return"></div>
        <div id="viewport" class="l-viewport">
            <div class="l-wrapper">
                <header class="header">
                    <a class="header--logo" href="#;">
                        <img src="/lib/global/img/Angel_Wing.png" alt="Global" width="30px">
                        <p>KANAPH</p>
                    </a>
                    {{--<button class="header--cta cta">SITE 바로가기</button>--}}
                    <div class="header--nav-toggle">
                        <span></span>
                    </div>
                </header>
                <nav class="l-side-nav">
                    <ul class="side-nav">
                        <li class="is-active"><span>메인</span></li>
                        <li><span>교회 및 위치</span></li>
                        <li><span>SNS</span></li>
                    </ul>
                </nav>
                <ul class="l-main-content main-content">
                    {{--<li class="l-section section section--is-active">
                        <div class="intro">
                            <div class="intro--banner">
                                <h1>KANAPH<br><br>God saw that<br> it was very good</h1>
                                <button class="cta">Hire Us
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 118" style="enable-background:new 0 0 150 118;" xml:space="preserve">
                  <g transform="translate(0.000000,118.000000) scale(0.100000,-0.100000)">
                      <path d="M870,1167c-34-17-55-57-46-90c3-15,81-100,194-211l187-185l-565-1c-431,0-571-3-590-13c-55-28-64-94-18-137c21-20,33-20,597-20h575l-192-193C800,103,794,94,849,39c20-20,39-29,61-29c28,0,63,30,298,262c147,144,272,271,279,282c30,51,23,60-219,304C947,1180,926,1196,870,1167z"/>
                  </g>
                  </svg>
                                    <span class="btn-background"></span>
                                </button>
                                <img src="/lib/global/img/introduction-visual.png" alt="Welcome">
                            </div>
                            <div class="intro--options">
                                <a href="#0">
                                    <h3>Metiew &amp; Smith</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do.</p>
                                </a>
                                <a href="#0">
                                    <h3>Fantasy interactive</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do.</p>
                                </a>
                                <a href="#0">
                                    <h3>Paul &amp; shark</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do.</p>
                                </a>
                            </div>
                        </div>
                    </li>--}}
                    <li class="l-section section section--is-active">
                        <div class="work">
                            <h2><span style="color: #ff4545;">W</span>elcome <span style="color: #ff4545;">T</span>o <span style="color: #ff4545;">K</span>ANAPH</h2>
                            <div class="work--lockup">
                                <ul class="slider">

                                    {{--<li class="slider--item slider--item-left">
                                        <a href="{{route('kanaph.calendar')}}">
                                            <div class="slider--item-image">
                                                <img src="/lib/global/img/images.jpg" alt="Metiew and Smith">
                                            </div>
                                            <p class="slider--item-title">Calender</p>
                                            <p class="slider--item-description">현재 진행되고 있는 일정으로 달력을 통해 확인 할 수 있습니다.</p>
                                        </a>
                                    </li>--}}

                                    <li class="slider--item slider--item-left">
                                        <a href="{{route('kanaph.info')}}">
                                            <div class="slider--item-image">
                                                <img src="/lib/global/img/info.jpg" alt="Victory">
                                            </div>
                                            <p class="slider--item-title">INFO</p>
                                            <p class="slider--item-description">카나프의 의미와 진행하고 있는 사역에 대한 정보를 확인 할 수 있습니다.</p>
                                        </a>
                                    </li>

                                    {{-- @if(session('group')['level'] > 0)--}}
                                    <li class="slider--item slider--item-center">
                                        <a href="{{route('bbs.list',['cidx'=>'1'])}}">
                                            <div class="slider--item-image">
                                                <img src="/lib/global/img/notice.png" alt="Alex Nowak">
                                            </div>
                                            <p class="slider--item-title">NOTICE</p>
                                            <p class="slider--item-description">공지사항 게시판입니다. 카나프에 소속 된 팀원들만 확인 할수 있습니다.</p>
                                        </a>
                                    </li>
                                    {{-- @endif--}}

                                    <li class="slider--item slider--item-right">
                                        <a href="{{route('bbs.list',['cidx'=>'2'])}}">
                                            <div class="slider--item-image">
                                                <img src="/lib/global/img/post.jpg" alt="Alex Nowak">
                                            </div>
                                            <p class="slider--item-title">POST</p>
                                            <p class="slider--item-description">카나프팀의 소소한 일상이야기를 블로그를 통해 여러분과 소통합니다.</p>
                                        </a>
                                    </li>

                                </ul>
                                <div class="slider--prev">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 150 118" style="enable-background:new 0 0 150 118;" xml:space="preserve">
                  <g transform="translate(0.000000,118.000000) scale(0.100000,-0.100000)">
                      <path d="M561,1169C525,1155,10,640,3,612c-3-13,1-36,8-52c8-15,134-145,281-289C527,41,562,10,590,10c22,0,41,9,61,29
                    c55,55,49,64-163,278L296,510h575c564,0,576,0,597,20c46,43,37,109-18,137c-19,10-159,13-590,13l-565,1l182,180
                    c101,99,187,188,193,199c16,30,12,57-12,84C631,1174,595,1183,561,1169z"/>
                  </g>
                  </svg>
                                </div>
                                <div class="slider--next">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 118" style="enable-background:new 0 0 150 118;" xml:space="preserve">
                  <g transform="translate(0.000000,118.000000) scale(0.100000,-0.100000)">
                      <path d="M870,1167c-34-17-55-57-46-90c3-15,81-100,194-211l187-185l-565-1c-431,0-571-3-590-13c-55-28-64-94-18-137c21-20,33-20,597-20h575l-192-193C800,103,794,94,849,39c20-20,39-29,61-29c28,0,63,30,298,262c147,144,272,271,279,282c30,51,23,60-219,304C947,1180,926,1196,870,1167z"/>
                  </g>
                  </svg>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="l-section section">
                        <div class="contact">
                            <div class="contact--lockup">
                                <div class="modal">
                                    <div class="modal--information">
                                        <p>서울특별시 성동구 성수일로 10길 33</p>
                                        <p style="color:#a0a4a7;">성수역 1번출구 10분거리</p>
                                        <a href="mailto:kanaph.wing@gmail.com">kanaph.wing@gmail.com</a>
                                        <a href="tel:010-8374-3696">010-8374-3696</a>
                                    </div>
                                    <ul class="modal--options">
                                        <li><a href="http://www.sungnak.org" target="_blank">성락성결교회</a></li>
                                        <li><a href="https://www.youtube.com/user/logoslifemedia" target="_blank">성락미디어</a></li>
                                        <li><a href="http://bosini.sungnak.org/"  target="_blank" style="background-color:#3c4682">보시니 묵상지</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="l-section section">
                        <div class="about">
                            <div class="about--banner">
                                <h2>SNS를<br>통해<br>카나프의 소식을<br>전해 들으세요.</h2>
                                {{--<a href="#0">Career</a>
                                    <span>
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 118" style="enable-background:new 0 0 150 118;" xml:space="preserve">
                                    <g transform="translate(0.000000,118.000000) scale(0.100000,-0.100000)">
                                      <path d="M870,1167c-34-17-55-57-46-90c3-15,81-100,194-211l187-185l-565-1c-431,0-571-3-590-13c-55-28-64-94-18-137c21-20,33-20,597-20h575l-192-193C800,103,794,94,849,39c20-20,39-29,61-29c28,0,63,30,298,262c147,144,272,271,279,282c30,51,23,60-219,304C947,1180,926,1196,870,1167z"/>
                                    </g>
                                    </svg>
                                  </span>
                                </a>--}}
                                <img src="/lib/global/img/slider-sns-img011-1.png" alt="About Us">
                            </div>
                            <div class="about--options">
                                <a href="https://www.instagram.com/kanaph_wing/" target="_blank">
                                    <h3>인스타그램</h3>
                                </a>
                                <a href="https://www.facebook.com/kanaph.wing/" target="_blank">
                                    <h3>페이스북</h3>
                                </a>
                                <a href="https://www.youtube.com/channel/UCI2eFOdtZzV00YT9Y-27T7w" target="_blank">
                                    <h3>유투브</h3>
                                </a>
                            </div>
                        </div>
                    </li>

                    <li class="l-section section">
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="outer-nav">
        <li class="is-active"><a href="#;">메인 메뉴</a></li>
        <li><a href="#;">교회 및 위치</a></li>
        <li><a href="#;">SNS</a></li>
        {{--<li><a href="https://www.youtube.com/channel/UCI2eFOdtZzV00YT9Y-27T7w">Youtube</a></li>--}}
        @if (Auth::guest())
        <li><a href="{{url('/login')}}">로그인</a></li>
        @else
        <li>
            <a href="{{ url('/logout') }}" class="en" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                로그아웃
            </a>
            <form id="logout-form" class="en" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
        @endif
    </ul>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/lib/global/js/vendor/jquery-2.2.4.min.js"><\/script>')</script>
<script src="/lib/global/js/functions-min.js"></script>
</body>
</html>
