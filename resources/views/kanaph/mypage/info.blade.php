@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('content')


    <style>

        .marketing > .col-lg-3, .jumbotron{
            text-align: center;
            margin-bottom: 30px;
        }

        .marketing h2{
            margin: 0px;

        }

        .my-img-update
        {
            margin-top: 20px;
        }

        .s{
            border: 1px solid #dedede;
            padding: 30px;
            border-radius: 30px;
        }

        .my-update{
            text-align: center;
        }

    </style>

{{--    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                        <li class="active">MYPAGE<i class="icon-angle-right"></i></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>--}}


    <div class="container">
        <br>
        <div class="jumbotron">
            <p class="lead"></p>
            @if(isset($profile->img) && $profile->img != '')
            <img class="img-circle" id="user-img" src="{{profile_img($profile->img)}}" alt="Generic placeholder image" width="300px">
            @endif
            <br>
            <p><a class="btn btn-lg btn-primary my-img-update" href="javascript:imgUpdate();" role="button">프로필 사진 변경하기</a></p>
            <div style="display: none;">
                <form  id="imgUpdateForm" method="post" action="{{route('mypage.img')}}"  enctype="multipart/form-data" target="_iframe" >
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <input name="img" type="file" id="img">
                </form>
            </div>
        </div>

        <div class="row marketing">
           {{-- <div class="col-lg-6">
                <h4>이름</h4>
                <p>{{Auth::user()['name']}}</p>


                <h4>파트</h4>
                <p>{{session('part')['info']}}</p>

                <h4>전화번호</h4>
                <p>작성안함</p>
            </div>

            <div class="col-lg-6">
                <h4>이메일</h4>
                <p>{{Auth::user()['email']}}</p>

                <h4>생년월일</h4>
                <p>작성안함</p>

                <h4>자기소개</h4>
                <p>작성안함</p>
            </div>--}}

            <div class="col-lg-7">

                <div class="col-lg-12">
                    <p><i class="fa fa-user"></i> 이름</p>
                    <h4>{{Auth::user()['name']}}</h4>
                    <hr>
                </div>
                <div class="col-lg-12">
                    <p> <i class="fa fa-envelope-o"></i> 이메일</p>
                    <h4>{{Auth::user()['email']}}</h4>
                    <hr>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-12">
                    <p> <i class="fa fa-users"></i> 파트 / 그룹</p>
                    <h4>{{session('part')['info']}} / {{session('group')['name']}}</h4>
                    <hr>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-12">
                    <p> <i class="fa fa-phone"></i> 전화번호</p>
                    <h4>{{$user->phone or '미작성'}}</h4>
                    <hr>
                </div>
                <div class="col-lg-12">
                    <p><i class="fa fa-calendar-o"></i> 생년월일</p>
                    <h4>{{$profile->birth or '미작성'}}</h4>
                    <hr>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="col-lg-12">
                    <p><i class="fa fa-pencil "></i> 자기소개</p>
                    <div class="s">{!!  $profile->info or '미작성'  !!}</div>
                    <hr>
                    <p class=" my-update"><a class="btn btn-lg btn-primary" href="{{route('mypage.edit')}}" role="button">개인정보 수정</a></p>
                </div>
            </div>

        </div>
    </div>


    <script>
        function imgUpdate(){
            $('#img').trigger('click');
        }

        $('#img').change(function(){
            if(confirm('정말 변경하시겠습니까?'))
            {
                $('#imgUpdateForm').submit();
            }
        });

    </script>
@endsection