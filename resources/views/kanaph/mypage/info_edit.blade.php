@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('content')

    <link href='/lib/bootstrap-datepicker/datepicker3.css' rel='stylesheet' />
    <style>
        .marketing > .col-lg-3, .jumbotron{
            text-align: center;
            margin-bottom: 30px;
        }

        .marketing h2{
            margin: 0px;
        }

        .my-img-update{
            margin-top: 20px;
        }

        .s{
            border: 1px solid #dedede;
            padding: 10px;
            border-radius: 30px;
        }

        .s > textarea {
            border : 0px;

        }


        .my-update{
            text-align: center;
        }

    </style>

    {{--<section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                        <li class="active">MYPAGE<i class="icon-angle-right"></i></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>--}}


    <div class="container">

        <br>

        <div class="jumbotron">
            <p class="lead"></p>
            @if(isset($profile->img) && $profile->img != '')
            <img class="img-circle" id="user-img"  src=" {{profile_img($profile->img)}} " alt="Generic placeholder image" width="300px">
            @endif
            <br>
            <p><a class="btn btn-lg btn-primary my-img-update" href="javascript:imgUpdate();" role="button">프로필 사진 변경하기</a></p>
            <div style="display: none;">
                <form  id="imgUpdateForm" method="post" action="{{route('mypage.img')}}"  enctype="multipart/form-data" target="_iframe" >
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <input name="img" type="file" id="img">
                </form>
            </div>
        </div>

        <div class="row marketing">
            <form  method="post" action="{{route('mypage.update')}}"  >
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PUT">
                <div class="col-lg-7">
                    <div class="col-lg-12">
                        <p> <i class="fa fa-envelope-o"></i> 이메일</p>
                        <h3>{{Auth::user()['email']}}</h3>
                        <hr>
                    </div>
                    <div class="col-lg-12">
                        <p><i class="fa fa-user"></i> 이름</p>
                        <input type="tel" class="form-control" name="name" value="{{$user->name or ''}}" placeholder="- 없이 입력" />
                        <hr>
                    </div>
                    <div class="col-lg-12">
                        <p> <i class="fa fa-phone"></i> 전화번호</p>
                        <input type="tel" class="form-control" name="phone" value="{{$user->phone or ''}}" placeholder="- 없이 입력" />
                        <hr>
                    </div>
                    <div class="col-lg-12">
                        <p><i class="fa fa-calendar-o"></i> 생년월일</p>
                        <div class='input-group '>
                            <input type="tel"  class="form-control date"  name="birth" value="{{$profile->birth or ''}}" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="col-lg-12">
                        <p><i class="fa fa-pencil "></i> 자기소개</p>
                        <h5 class="s">
                            <textarea name="info" rows="10" class="form-control">{{$profile->info or ''}}</textarea>
                        </h5>
                        <hr>
                        <p class=" my-update">
                            <button class="btn btn-lg btn-primary" type="submit" role="button">개인정보 수정</button>
                            <a href="{{route('mypage.index')}}" class="btn btn-lg btn-default" role="button">뒤로</a>
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src='/lib/bootstrap-datepicker/bootstrap-datepicker.js'></script>
    <script src='/lib/bootstrap-datepicker/bootstrap-datepicker.kr.js'></script>
    <script>
        function imgUpdate(){
            $('#img').trigger('click');
        }

        $('#img').change(function(){
            if(confirm('정말 변경하시겠습니까?'))
            {
                $('#imgUpdateForm').submit();
            }
        });

        $('.date').datepicker({
            format: "yyyy-mm-dd",
            language: "kr"
        });

    </script>
@endsection