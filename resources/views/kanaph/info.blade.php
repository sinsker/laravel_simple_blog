@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('content')

<link href="/css/kanaph/info.team.css" rel="stylesheet">

{{--<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                    <li><a href="#">INFO</a><i class="icon-angle-right"></i></li>
                    <li class="active">About Kanaph</li>
                </ul>
            </div>
        </div>
    </div>
</section>--}}
<!-- Full Width Image Header -->
<header class="header-image">
    <div class="headline">
        <div class="container" style="z-index: 600;">
            <h1 class="en">KANAPH<span style="color: #73dcf5;">?</span> KANAPH<span style="color: #73dcf5;">!</span></h1>
            <h2> :  כנף : 날개 :</h2>
        </div>
        <div class="socket" >
            <div class="gel center-gel">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c1 r1">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c2 r1">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c3 r1">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c4 r1">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c5 r1">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c6 r1">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>

            <div class="gel c7 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>

            <div class="gel c8 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c9 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c10 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c11 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c12 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c13 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c14 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c15 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c16 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c17 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c18 r2">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c19 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c20 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c21 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c22 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c23 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c24 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c25 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c26 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c28 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c29 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c30 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c31 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c32 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c33 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c34 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c35 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c36 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
            <div class="gel c37 r3">
                <div class="hex-brick h1"></div>
                <div class="hex-brick h2"></div>
                <div class="hex-brick h3"></div>
            </div>
        </div>
    </div>

</header>

<section id="content">
    <!-- Page Content -->
    <div class="container">
        <hr class="featurette-divider">

        <!-- First Featurette -->
        <div class="featurette" id="about">
            <img class="featurette-image img-circle img-responsive pull-right" src="/images/kanaph/_1hdUd015aufd9sv9fqoh_qg59pa.jpg">
            <h2 class="featurette-heading">KANAPH(카나프)</h2>
            <h4 class="text-muted">문화사역팀</h4>
            <p class="lead">"카나프"란 히브리어로 날개라는 뜻으로 세상의 문화를 통해 쉽게 접하고 다가갈수 있도록 춤ㆍ드라마ㆍ컨텐츠 등으로 복음을 전파하는 문화사역팀입니다.</p>
        </div>

        <hr class="featurette-divider">

        <!-- Second Featurette -->
        <div class="featurette" id="services">
            <img class="featurette-image img-circle img-responsive pull-left" src="/images/kanaph/2h7Ud0156u91znzxi46b_uk5m8o.jpg" width="500px;">
            <h2 class="featurette-heading">Creative Part
            </h2>
            <h4 class="text-muted">안무 창작 및 연습</h4>
            <p class="lead">
                팀 내에서 안무 창작 및 연습을 담당하고 있으며, 주로 매주 3번째 주에 있는 스탠딩 예배의 댄스 워십, 부활절 혹은 성탄절 등 특송의 준비를 담당하는 파트입니다.
            </p>
        </div>

        <hr class="featurette-divider">

        <!-- Third Featurette -->
        <div class="featurette" id="contact">
            <img class="featurette-image img-circle img-responsive pull-right" src="/images/kanaph/82dUd0151pvxjbxm5rm9b_cwtqeh.jpg" width="500px;">
            <h2 class="featurette-heading">Mission Part
            </h2>
            <h4 class="text-muted">말씀 훈련</h4>
            <p class="lead">
                팀 내에서 영적인 미션을 주고 있으며 주로 묵상 나눔, 기도, 전도 등을 통해 문화사역 뿐만 아니라 개개인의 신앙적 훈련을 지속적으로 이끌어 주는 파트입니다.
            </p>
        </div>


        <hr class="featurette-divider">

        <!-- Third Featurette -->
        <div class="featurette" id="contact">
            <img class="featurette-image img-circle img-responsive pull-left" src="/images/kanaph/486Ud0157oqwjfh3xh2p_iva4nd.jpg" width="500px;">
            <h2 class="featurette-heading">Community Part
            </h2>
            <h4 class="text-muted">컨텐츠 홍보</h4>
            <p class="lead">
                팀에서 진행 되는 모든 사역 진행에 대해 영상이나 공유할수 있는 사진으로 컨텐츠를 만들어 일반 사람들이 쉽게 접근 할 수 있도록 SNS 나 사이트로 홍보하는 파트입니다.
            </p>
        </div>
        <hr class="featurette-divider">

    </div>
</section>
@endsection