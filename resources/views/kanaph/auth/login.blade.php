@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')


@section('layout_top')
    {!! $og->template() !!}
    <link href="/css/kanaph/login.css" rel="stylesheet" />

@endsection

@section('content')
    <style>
        .btn{
            height: 44px !important;
        }

    </style>

    <section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                    <li><a href="#">Login</a><i class="icon-angle-right"></i></li>
                </ul>
            </div>
        </div>
    </div>
    </section>

    <div class="auth-contaniner">
        <div class="card card-container">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class=" form-signin form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <span id="reauth-email" class="reauth-email"></span>
                <input type="email" id="inputEmail" name="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" placeholder="Email"  autofocus>
                <span class="help-block">
                    <strong class="">{{ $errors->first('email') }}</strong>
                </span>
                <input type="password" id="inputPassword" name="password" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" placeholder="Password" required>
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}>카나프계정 저장
                            </label>
                        </div>
                    </div>
                </div>
                {{--<div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox"  name="remember" {{ old('remember') ? 'checked' : ''}}>기억하기
                    </label>
                </div>--}}
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">로그인</button>
            </form><!-- /form -->
            <button  class="btn btn-lg btn-default btn-block" onclick="javascript:location.href = '{{url('/register')}}'"  >회원가입</button>
            <a href="javascript:loginWithKakao()" style="margin-top: 5px; display: inline-block" >
                <img  src="https://developers.kakao.com/assets/img/about/logos/login/kr/kakao_account_login_btn_large_wide_ov.png">
            </a>
            <a href="javascript:loginWithFaceback()" class="btn btn-block loginBtn loginBtn--facebook">페이스북으로 로그인</a>
            {{--<a href="javascript:loginWithFaceback()" class="btn btn-block loginBtn loginBtn--google">구글계정으로 로그인</a>--}}
        </div><!-- /card-container -->
    </div><!-- /container -->
@endsection


@section('layout_footer')
    <script type='text/javascript'>
        //<![CDATA[

        // 카카오 로그인 버튼을 생성합니다.
        function loginWithKakao() {
            // 로그인 창을 띄웁니다.
            Kakao.Auth.login({
                success: function(authObj) {

                    var data = {
                        'type' : 'kakao',
                        'access_token' : authObj.access_token,
                        'token_type' : authObj.token_type
                    };

                    authLogin(data);

                },
                fail: function(err) {

                    alert('카카오톡 로그인에 문제가 발생하였습니다. 다른 방식으로 로그인 해주시기 바랍니다.');

                }
            });
        }

        function loginWithFaceback(){
            FB.getLoginStatus(statusCallbackFacebook);
        }


        function statusCallbackFacebook(response){
            if (response.status === 'connected') {
                /*FB.api('/me', {fields:'name, email' },function(response) {
                });*/

                facebookAuthenticationComplete(response);

            }else{
                FB.login(function(response) {
                    if (response.authResponse) {
                        facebookAuthenticationComplete(response);
                    } else {
                        //fail

                    }
                },{scope:'email',return_scopes: true});
            }
        }

        //페이스북 인증 성공
        function facebookAuthenticationComplete(response){
            var accessToken = response.authResponse.accessToken;
            var data = {
                'type' : 'facebook',
                'access_token' : accessToken
            };
            authLogin(data)
        }

        //인증
        function authLogin(data)
        {
            var post = {
                'type'         : data['type'],
                'access_token' : data['access_token'],
                '_token'       : '{{csrf_token()}}'
            };

            $.post('{{route('login.oauth')}}', post ,function(data){
                if(data.massage) alert(data.massage);
                if(data.result)
                {
                    location.href = data.referer;
                }
            });
        }
        //]]>
    </script>
@endsection