@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('content')

@section('layout_top')
    <link href="/css/kanaph/register.css" rel="stylesheet" />
@endsection

<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                    <li><a href="#">Register</a><i class="icon-angle-right"></i></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<div class="auth-contaniner">
    <div class="card card-container">
        <form class="form-horizontal form-signin" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-3 control-label">이름</label>
                <div class="col-md-9">
                    <input id="name" type="text" class="form-control" name="name" required placeholder="이름"  required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-3 control-label">이메일</label>
                <div class="col-md-9">
                    <input id="email" type="email" class="form-control" name="email" required placeholder="이메일" autofocus required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>


            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-3 control-label">비밀번호</label>
                <div class="col-md-9">
                    <input id="email" type="password" class="form-control" name="password"  placeholder="비밀번호" required  required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-3 control-label">비밀번호 재확인</label>
                <div class="col-md-9">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="비밀번호 재확인" required>
                </div>
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="email" class="col-md-3 control-label">전화번호</label>
                <div class="col-md-9">
                    <input id="email" type="tel" class="form-control" name="phone" required placeholder="'-' 없이 입력" autofocus required>
                    @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">회원가입</button>
        </form><!-- /form -->
        <a href='{{url('/login')}}' class="forgot-password">
            로그인으로 돌아가기
        </a>
    </div><!-- /card-container -->
</div><!-- /container -->
@endsection


{{--
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">이름</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">휴대전화</label>
                            <div class="col-md-2">
                                <select name="phone_code" class="form-control">
                                    <option>코드</option>
                                    <option value="82">+82</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <input id="phone" type="phone" class="form-control" name="phone" value="{{ old('phone') }}" required>

                            @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">비밀번호</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">비밀번호 재확인</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    확인
                                </button>
                                <button type="button" class="btn btn-default" onclick="javascript:location.ref={{ url('/login') }} ">
                                    취소
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}