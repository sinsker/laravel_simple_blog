@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('content')

    <link href='/lib/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
    <link href='/lib/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link href='/lib/bootstrap-datepicker/datepicker3.css' rel='stylesheet' />

    <style>
        .fc-left > h2 {
            font-size: 20px;
        }

        .fc-content{
            padding: 3px;
        }

    </style>

    
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href="/"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                        <li class="active">Canendar</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <br><br>

    <div class="container">
        <div class="row">

            <div class="col-md-12" >

                <div id='calendar'></div>
            </div>
			<br>
        </div>
    </div>

	<div class="modal fade" id="scheduleEdit" >
	  <div class="modal-dialog">
      <form id="procForm" class="form-horizontal" method="post" action="{{route('kanaph.calendar.add')}}">
          {{ csrf_field() }}
	    <div class="modal-content">
	      <div class="modal-header">
	        <!-- 닫기(x) 버튼 -->
	        <button type="button" class="close" data-dismiss="modal">×</button>
	        <!-- header title -->
	        <h4 class="modal-title">새로운 일정 추가</h4>
	      </div>
	      <!-- body -->
	      <div class="modal-body">
                <div class="form-group">
                   <!--  <label for="inputWrite" class="col-sm-2 control-label">옵션</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="option[]" >반짝반짝
                        <input type="checkbox" name="option[]" >특송
                        <input type="checkbox" name="option[]" >
                    </div> -->
                </div>
                <div class="form-group">
                    <label for="inputWrite" class="col-sm-2 control-label">제목</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="s_title" name="s_title">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputWrite" class="col-sm-2 control-label">내용</label>
                    <div class="col-sm-10">
                        <textarea rows="3" cols="3" class="form-control" id="s_content" name="s_content" ></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputWrite" class="col-sm-2 control-label">시작일</label>
                    <div class="col-sm-10">
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control" id='datestart' name="start" value="{{date('Y-m-d')}}" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputWrite" class="col-sm-2 control-label">종료일</label>
                    <div class="col-sm-10">
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control" id='dateend' name="end" value="{{date('Y-m-d')}}"  />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
	      </div>
	      <!-- Footer -->
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary" >추가</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
	      </div>
	    </div>
	      	</form>
	  </div>
	</div>

    <div class="modal fade" id="scheduleView" >
        <div class="modal-dialog">
            <form id="procForm" class="form-horizontal" method="post" action="">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <!-- 닫기(x) 버튼 -->
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <!-- header title -->
                        <h4 class="modal-title" id="v_title"></h4>
                    </div>
                    <!-- body -->
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="inputWrite" class="col-sm-2 control-label">일정</label>
                            <div class="col-sm-10">
                                <span id="v_start"></span> ~ <span id="v_end"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <pre id="v_content" style="padding-top: 15px; padding-bottom: 15px; font-family: Godo" ></pre>
                            </div>
                        </div>
                    </div>
                    <!-- Footer -->

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" >삭제</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
	

    <script src='/lib/fullcalendar/lib/moment.min.js'></script>
    <script src='/lib/fullcalendar/fullcalendar.min.js'></script>
    <script src='/lib/fullcalendar/locale-all.js'></script>
    <script src='/lib/bootstrap-datepicker/bootstrap-datepicker.js'></script>
    <script src='/lib/bootstrap-datepicker/bootstrap-datepicker.kr.js'></script>
    
    <script>

    var schedules = [
    @if(isset($schedules))
        @foreach($schedules as $item)
            {
                title: '{{$item->s_title}}',
                id: '{{$item->idx}}',
                start: '{{date('Y-m-d',strtotime($item->start))}}',
                end: '{{date('Y-m-d',strtotime($item->end))}}',
                color:'#59c9f1'
            },
        @endforeach
    @endif
    ];

    $(document).ready(function() {

        $('#calendar').fullCalendar({
            defaultDate: '{{date('Y-m-d')}}',
            @if(session('group')['level'] > 0)
            customButtons: {
                add: {
                    text: '추가',
                    click: function() {
                        $('#scheduleEdit').modal();
                }
                }
            },
            @endif
            header: {
                left: 'title, today',
                right: 'add, month,prev,next'
            },
            height: 'auto',
            locale : 'ko',
            editable: false,
            eventLimit: false, // allow "more" link when too many events
            navLinks: true, // can click day/week names to navigate views
            businessHours: true, // display business hours
            events : schedules,

            eventClick: function(calEvent, jsEvent, view) {

                var schedule = getSchedule(calEvent.id);

                $('#v_title').html(schedule.s_title);
                $('#v_content').html(schedule.s_content);
                $('#v_start').html(schedule.start_m+'(' + schedule.start_w +')');
                $('#v_end').html(schedule.end_m+'(' + schedule.end_w +')');

                $('#scheduleView').modal();

            }
        });

        $('#datestart').datepicker({
             format: "yyyy-mm-dd",
             language: "kr"
        });

        $('#dateend').datepicker({
            format: "yyyy-mm-dd",
            language: "kr"
       });

        $('#datestart').change(function(){

            if( $('#dateend').val() == '' || $('#dateend').val() < $('#datestart').val() ){
                $('#dateend').val($(this).val());
            }
        });

    });

    function getSchedule(idx)
    {
        var schedule = {};

        var datas = {
            _type: 'json',
            _data_only: true
        };

        $.ajax({
            url : "{{route('kanaph.calendar')}}/"+idx,
            type : "get",
            async: false,
            data : datas,
            dataType : 'json',
            success : function(data){
                schedule = data.schedule;
            }
        });

        return schedule;
    }
    </script>
@endsection