<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <title>KANAPH - 문화사역팀</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="카나프, kanaph, 성락, 성락성결교회, sync, 싱크">
    <meta name="keywords" content="카나프, kanaph, 성락, 성락성결교회, sync, 싱크, ccd, 특송, 워십, 문화사역, 사역">

    <link rel="shortcut icon" href="/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Luckiest+Guy" rel="stylesheet">
    <link href="/css/kanaph/bootstrap.min.css" rel="stylesheet" />
    <link href="/css/kanaph/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="/css/kanaph/jcarousel.css" rel="stylesheet" />
    <link href="/css/kanaph/flexslider.css" rel="stylesheet" />
    <link href="/css/kanaph/style.css" rel="stylesheet" />
    <link href="/css/kanaph/default.css" rel="stylesheet" />

    <script src="/js/kanaph/jquery.js"></script>
    <script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
    <script src="/js/kanaph/kanaph.js?{{time()}}"></script>
    @yield('layout_top')
</head>
<body>

<div id="wrapper" >
    <div class="bg_w">
    @yield('top')
    </div>
@yield('content')
@yield('footer')
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="/js/kanaph/jquery.easing.1.3.js"></script>
<script src="/js/kanaph/bootstrap.min.js"></script>
<script src="/js/kanaph/jquery.fancybox.pack.js"></script>
<script src="/js/kanaph/jquery.fancybox-media.js"></script>
<script src="/js/kanaph/google-code-prettify/prettify.js"></script>
<script src="/js/kanaph/portfolio/jquery.quicksand.js"></script>
<script src="/js/kanaph/portfolio/setting.js"></script>
<script src="/js/kanaph/jquery.flexslider.js"></script>
<script src="/js/kanaph/animate.js"></script>
<script src="/js/kanaph/custom.js"></script>
@yield('layout_footer')
<script>
    $(function(){
        if( $('#error-alert').length > 0){
            $('#error-alert').delay(4000).slideUp(400);
        }
    });
</script>
<iframe id="_iframe" name="_iframe" style="display: none;"></iframe>
</body>
</html>

