@section('footer')
<footer>

   <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="widget" style="text-align: center;">
                        주님에 대한 열정으로 넘치는<br>
                        KANAPH가 함께 합니다♥
                </div>
                <br><br>
                <ul class="social-network" style="text-align: center;font-size: 12px;color: #7d7d7d;">
                    <li><a href="/main" data-placement="top" title="그외 정보"><i class="fa fa-flash"></i></a></li>
                    <li><a href="https://www.instagram.com/kanaph_wing/" target="_blank" data-placement="top" title="인스타그램"><i class="fa fa-linkedin"></i></a></li>
                    <li>Copyright© 2017 By Sinil all rights reserved.</li>
                </ul>
            </div>
            <br>
        </div>
    </div>
</footer>
@endsection