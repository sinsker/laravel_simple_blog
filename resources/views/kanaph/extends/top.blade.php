@section('top')
<header>
    <div class="navbar navbar-default navbar-static-top">
        <div class="menu">
            <div class="navbar-header" style="padding: 10px;">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand glitch" href="{{ route('kanaph.index') }}" style="color: #252525;"><span>K</span> a n a p h</a>
            </div>
            <div class="navbar-collapse collapse ">
                <ul class="nav navbar-nav">
                    {{--
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle en" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">
                            Info <span class="caret en" style="border-bottom-color: #000000; border-top-color: #6f6f6f;"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="en" href="{{ route('kanaph.info') }}" >About Kanaph</a></li>
                            --}}{{--<li><a class="en" href="{{ route('kanaph.info.member') }}">Team Members</a></li>--}}{{--
                        </ul>--}}

                    <li><a class="en" href="{{ route('kanaph.info') }}" >About Kanaph?</a><li>
                    {{--<li><a class="en" href="{{route('bbs.list',['cidx'=>'1'])}}" >Notice</a><li>--}}
                    <li><a class="en" href="{{route('kanaph.calendar')}}">Calendar</a><li>
                    {{--<li><a href="https://www.youtube.com/channel/UCI2eFOdtZzV00YT9Y-27T7w--}}{{--{{route('kanaph.movie')}}--}}{{--" class="en">Movie</a></li>--}}
                    <li><a href="https://www.youtube.com/channel/UCI2eFOdtZzV00YT9Y-27T7w" target="_blank" class="en">Youtube</a><a href="#;" class="en" style="  margin-left: 3px; "/>/</a><a href="https://www.instagram.com/kanaph_wing/" target="_blank" class="en" style="  margin-left: 3px; ">Instagram</a></li>
                    <li><a href="{{route('bbs.list',['cidx'=>'2'])}}" class="en">Post</a></li>

                <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li class="active">
                            <a class="user-toggle en" href="{{ url('/login') }}" style="display: inline-block;">Login</a>
                        </li>
                    @else
                        <li class="dropdown active">
                            <a href="#" class="dropdown-toggle user-toggle en" data-toggle="dropdown" data-hover="dropdown" role="button" aria-expanded="false" style="text-transform: none;" >
                                {{ Auth::user()->name}} (@if((Auth::user()->oauth_type)) {{Auth::user()->oauth_type.'로그인'}}  @else {{Auth::user()->email}}  @endif ) <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                @if(session('group')['level'] > 3)
                                <li><a href="{{ route('admin.kanaph') }}" class="en bg-info">Admin</a></li>
                                @endif
                                <li><a href="{{ route('mypage.index') }}" class="en">MyPage</a></li>
                                <li>
                                    <a href="{{ url('/logout') }}" class="en" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" class="en" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</header>
@if($errors->any())
    <div class="alert alert-danger" id="error-alert" role="alert" style="margin-bottom:0px;">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Fail:</span>
        @foreach($errors->all() as $message)
            {{$message}}
        @endforeach
    </div>
@endif
@endsection