@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('content')

    <link href="/css/kanaph/bbs.post.css" rel="stylesheet">

    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Fail:</span>
            @foreach($errors->all() as $message)
                {{$message}}
            @endforeach
        </div>
    @endif


    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                        <li class="active">TAG</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h3>"{{$id}}" 검색 결과 ({{$list->count()}})</h3>
                    <hr>
                    @if($list->count() > 0)
                    @foreach($list as $item)
                    <div >
                        <a href="{{route('bbs.view',['cidx'=> $item->bc_idx, 'idx'=>$item->idx])}}">
                        <h4>{{$item->title}}</h4>
                        <span class="pullquote-left">
                            {!!$item->content!!}
                        </span>
                        </a>
                        <br>
                    </div>
                    @endforeach
                    @endif
                </div>

                <br>
            </div>
        </div>
    </section>
@endsection