@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('layout_top')
    {!! $og->template() !!}

    <link href="/css/kanaph/bbs.board.css" rel="stylesheet">
@endsection

@section('content')
    {{-- <section id="inner-headline">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="breadcrumb">
                            <li><a href="/"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                            <li class="active en" ><a href="{{route('bbs.list',['cidx'=>$boardConfig->idx])}}">NOTICE</a></li>
                            <li class="active en">VIEW</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>--}}

    <div class="container" style="margin-top: 10px;">
        <div class="row">

            <div class="col-md-8 mb_full bg_w" >
                <h4 style="padding: 15px 5px 0px 5px;">{{$board->title}}</h4>
                <div id="bskr-view">
                    <div class="viewbox">
                        <div class="header">
                            <div class="icon " style="background: none;">
                                <img class="profile-img" src="{{profile_img($board->user_img)}}" width="50px;" style="height:50px;">
                            </div>
                            <div class="subject">
                                <h1 class="bskr-font-xlg">
                                    <span class="cat">
                                        <span class="han">
                                        {{$board->name}}
                                    </span>
                                    <span class="split">&nbsp;|&nbsp;</span>
                                        {{$board->date}}

                                    </span>
                                    <span style="float:right;">
                                       <div class="dropdown bbs-submenu">
                                          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                            <i class="fa fa-caret-down"></i>
                                          </button>
                                          <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                            <li role="presentation">
                                                <a role="menuitem" tabindex="-1" href="javascript:share_kakao('{{$board->title}}','{{$og->get('description')}}','{{url($_SERVER['REQUEST_URI'])}}','{{$board->like_cnt}}','{{$board->reply_cnt}}')">
                                                    &nbsp;<img src="https://developers.kakao.com/assets/img/about/logos/kakaolink/kakaolink_btn_small_ov.png" style="width: 15px;">&nbsp;
                                                    카카오톡 공유
                                                </a>
                                            </li>
                                            <li role="presentation">
                                                <a role="menuitem" tabindex="-1" href="javascript:share_facebook('{{url($_SERVER['REQUEST_URI'])}}');">
                                                    <img src="/images/share/facebook-icon-preview.png" style="width: 20px;">
                                                    페이스북 공유
                                                </a>
                                            </li>
                                            <li role="presentation">
                                                <a role="menuitem" tabindex="-1" href="javascript:share_url('{{url($_SERVER['REQUEST_URI'])}}');">
                                                    &nbsp; <i class="fa fa-share-square" aria-hidden="true"></i>&nbsp;
                                                    링크 복사
                                                </a>
                                            </li>
                                          </ul>
                                        </div>
                                    </span>
                                    {{-- <div class="snsbox" style="    float: right;">
                                         <i class="fa fa-google-plus"></i>&nbsp;
                                         <i class="fa fa-facebook"></i>&nbsp;
                                         <i class="fa fa-instagram"></i>
                                     </div>--}}
                                </h1>
                            </div>
                            <div class="info">
                                {{--<div class="xleft bskr-font-lg">

                                    --}}{{--<span class="split">|</span>
                                    <span class="han"><i class="fa fa-eye"></i></span>
                                    <span class="num">{{$board->hit}}</span>--}}{{--
                                </div>--}}

                                <div class="xleft ">
                                    <span class="like-content" data-idx="{{$board->idx}}">
                                    @if(isset($board->liked) &&$board->liked == true)
                                            <a href="#;" class="liked"><i class="fa fa-heart"></i> <span class="en">좋아요 취소</span></a>
                                        @else
                                            <a href="#:" class="like"><i class="fa fa-heart-o"></i> <span class="en">좋아요</span></a>
                                        @endif
                                     </span>
                                    @if($board->like_cnt > 0)
                                        <span @if(isset($board->liked) &&$board->liked == true) class="liked" @endif>
                                        <span class="like-cnt en"><span>{{$board->like_cnt}}</span>개</span>
                                    </span>
                                    @endif
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <div id="vContent" class="content">
                            <!-- 파일 -->
                            @if($files->count() >0 )
                                <div class="attach">
                                    <ul>
                                        @foreach($files as $file)
                                            <li>
                                                <i class="fa fa-file"></i>
                                                <a href="{{route('down.single',['idx'=>$file->idx])}}" title="{{$file->oriname}}" class="en">{{$file->oriname}}</a>
                                                <span class="size">({{file_size($file->size)}})</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        <!-- 파일 -->
                            {!! $board->content !!}
                            {{--<div class="scorebox">
                                <a href="/?c=forum/qna&amp;a=score&amp;value=good&amp;uid=255" target="_action_frame_bbs" onclick="return confirm('정말로 평가하시겠습니까?');" class="btn btn-success">
                                    <i class=""></i> 좋아요<span id="up255">
                                    </span>
                                </a>
                            </div>--}}


                            <div class="widget" style="text-align: right;">
                                <ul class="tags">
                                    @if(isset($board->tagList))
                                        @foreach($board->tagList as $item)
                                            <li><a href="{{route('tag.list', [/*'cidx'=>$boardConfig->idx,*/'tag' => $item])}}">{{$item}}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div style=" display: inline-block;width: 100%;">
                            <div class="right-side">
                                @if($board->writer_idx == Auth::id())
                                    <a href="{{route('bbs.edit',['idx'=>$board->idx,'cidx'=>$boardConfig->idx] )}}"  class="btn btn-default">수정</a>
                                    <form method="post"  action="{{route('bbs.delete',$board->idx)}}" style="display:inline-block;">
                                        {{ csrf_field() }}
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button type="submit"  class="btn btn-default">삭제</button>
                                    </form>
                                @endif
                                <a href="{{route('bbs.list',['cidx'=>$board->bc_idx])}}" class="btn btn-default">목록</a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                <div class="">
                    @include('kanaph.bbs.post.reply')
                </div>
            </div>

            <div class="col-md-4 mb_full mb-hide" >
                <aside class="right-sidebar">
                    <div class="widget">
                        <h5 class="widgetheading collapsed" data-toggle="collapse"  href="#cate-list" aria-expanded="false" aria-controls="cate-list">카테고리 분류</h5>
                        <ul class="cat" id="cate-list">
                            @if(isset($categories) && count($categories) > 0)
                                @foreach($categories as $item)
                                    <li><i class="icon-angle-right"></i><a href="#">{{$item->cate}}</a><span> ({{$item->cate_row}})</span></li>
                                @endforeach
                            @else
                                없음
                            @endif
                        </ul>
                    </div>
                    <div class="widget">
                        <h5 class="widgetheading collapsed" data-toggle="collapse"  href="#lastest-list" aria-expanded="false" aria-controls="lastest-list">최근 등록된 게시글</h5>
                        <ul class="recent" id="lastest-list">
                            @if(isset($latestItems) && count($latestItems) > 0)
                                @foreach($latestItems as $latestItem)
                                    <li style="/*display: inline-block; width: 100%;*/">
                                        {!! $latestItem->src or '' !!}
                                        <a href="{{route('bbs.view', ['cidx'=>$boardConfig->idx,'idx'=>$latestItem->idx])}}">
                                            <p>
                                                {!! $latestItem->content or '' !!}
                                            </p>
                                            <h6>{{$latestItem->title}} <span style="float: right; color: #b1b1b1; ">{{$latestItem->date}}</span></h6>
                                        </a>
                                    </li>
                                @endforeach
                            @else
                                없음
                            @endif
                        </ul>
                    </div>
                    <div class="widget">
                        <h5 class="widgetheading">태그</h5>
                        <ul class="tags">
                            @if(isset($tags) && count($tags) > 0)
                                @foreach($tags as $tag)
                                    <li><a href="{{route('tag.list', ['tag' => $tag->id])}}">{{$tag->id}}</a></li>
                                @endforeach
                            @else
                                없음
                            @endif
                        </ul>
                    </div>
                </aside>
            </div>

        </div>
    </div>


    <script>

        $('.like').click(function(){

            board_like($(this).parent().data('idx'));
        });

        $('.liked').click(function(){

            board_unlike($(this).parent().data('idx'));
        });

        function board_like(board_idx){

            $.get("/like/"+board_idx+"/love",function(data){

                if(data.like){
                    var love = $('.like-content[data-idx="'+board_idx+'"]');

                    love.html(likeHtmlTemplate('unlike'));
                    love.next().addClass('liked');
                    love.next().find('span.like-cnt > span').text(Number(love.next().find('span.like-cnt> span').text())+1);

                    love.find('.liked').click(function(){
                        board_unlike(board_idx);
                    })
                }

            },"json");
        }


        function board_unlike(board_idx){

            $.get("/like/cancel/"+board_idx,function(data){

                if(data.unlike){
                    var love = $('.like-content[data-idx="'+board_idx+'"]');

                    love.html(likeHtmlTemplate('like'));
                    love.next().removeClass('liked');
                    love.next().find('span.like-cnt > span').text(Number(love.next().find('span.like-cnt> span').text())-1);

                    love.find('.like').click(function(){
                        board_like(board_idx);
                    })
                }

            },"json");

        }


        function likeHtmlTemplate(type)
        {

            var html = '';

            switch (type){

                case 'like':
                    html = '<a href="#:" class="like"><i class="fa fa-heart-o"></i> <span class="en">좋아요</span></a>';
                    break;

                case 'unlike':
                    html = '<a href="#;" class="liked"><i class="fa fa-heart"></i> <span class="en">좋아요 취소</span></a>';
                    break;
            }

            return html;
        }
    </script>
@endsection

