<div style="margin-top: 30px;">
    <form class="form-horizontal" method="post" action="{{route('reply.store')}}" onsubmit="return procForm(this);">
        {{ csrf_field() }}
        <input type="hidden" name="bidx" value="{{$board->idx}}">
        <input type="hidden" name="bc_idx" value="{{$board->bc_idx}}">
        <div class="panel-comment" style="    margin-bottom: 60px;">
            <textarea class="form-control" name="content" style="margin-bottom: 15px;"></textarea>
            <div class="right-side">
                <button class="btn btn-primary ">
                    댓글 등록
                </button>
            </div>
        </div>
    </form>
</div>

<div id="reply-box" ></div>

<script>
    $(function(){

        $.get('{{route('reply.list',['bidx'=>$board->idx] )}}', function(result){
            var html = '';
            for( i in result.replys){
                html += templateReplayView(result.replys[i]);
            }

            $('#reply-box').html(html);

            $('.reply-update-btn').on('click',function(i){

                var commentContent = $(this).parent().parent().next();

                var idx     =  commentContent.data('idx');
                var content =  commentContent.children().html();

                updateView({'idx':idx,'content':content});
            });

        });

    });

    //댓글 submit action
    function procForm(form){

        if(form.content.value == ''){
            alert('댓글을 입력해주시기 바랍니다.');
            return false;
        }

        return true;

    }

    //리플 업데이트 뷰
    function updateView(datas){
        var template = templateReplayUpdateView(datas);

        updateViewClose();

        if(!$('[data-idx='+datas.idx+']').hasClass('update')){

            $('[data-idx='+datas.idx+']').addClass('update');

            $('[data-idx='+datas.idx+']').append(template);
        }

    }

    //리플 업데이트 뷰 닫기
    function updateViewClose(){
        $('.reply-content').removeClass('update');
        $('.reply-update-view').remove();
    }
    function deleteReply(idx){
        if(confirm('정말삭제하시겠습니까?')){
            var data = {
                '_method' : 'DELETE',
                'idx'     : idx,
                'bc_idx'  : {{$board->bc_idx}},
                '_token'  : '{{ csrf_token()}}'
            };

            $.post('{{route('reply.delete')}}', data, function(result){
                location.href = '{{route('bbs.view',['bc_idx'=>$board->bc_idx,'idx'=>$board->idx])}}';
            });

        }
    }

    //리플 템플릿
    function templateReplayView(datas){
        var html = '';
        html += '<div class="panel panel-comment panel-default " style="margin-bottom: 2px">';
        html += '<div class="panel-heading-comment panel-heading">';
        html += '<img class="profile-img" src="'+datas.user_img+'" width="25px;" style="height:25px;">&nbsp;<strong>'+ datas.name +'</strong> <span class="text-muted bskr-font">| '+datas.date+'</span>'; //
        if(datas.possession == true)
        {
            html += '<span class="text-right" style="float:right">' +
                        '<button class="btn btn-xs btn-default reply-update-btn">수정</button>&nbsp;' +
                        '<button class="btn btn-xs btn-default" onclick="deleteReply('+datas.idx+')">삭제</button>' +
                    '</span>';
        }
        html += '</div>';
        html += '<div class="panel-body reply-content"  data-idx="'+datas.idx+'"  >';
        html += '<div class="reply-content-text">';
        html +=  datas.content;
        html += '</div>';
        html += '</div>';
        html += '</div>';

        return html;
    }

    //리플 업데이트 템플릿
    function templateReplayUpdateView(datas){
        var html = '';
        html +='<div class="row reply-update-view">';
        html +='    <form class="form-horizontal" method="post" action="{{route('reply.update')}}" onsubmit="return procForm(this);" >';
        html +='       {{ csrf_field() }}';
        html +='       <input type="hidden" name="idx" value="'+datas.idx+'">';
        html +='       <input type="hidden" name="bc_idx" value="{{$board->bc_idx}}">';
        html +='       <input type="hidden" name="_method" value="PUT">';
        html +='       <input type="hidden" name="bidx" value="{{$board->idx}}">';
        html +='       <div class="col-xs-12">';
        html +='            <div class="panel-comment">';
        html +='                <textarea class="form-control" name="content">'+datas.content+'</textarea>';
        html +='                <button class="btn btn-block btn-primary">수정</button>';
        html +='            </div>';
        html +='        </div>';
        html +='    </form>';
        html +='</div>';

        return html;
    }
</script>