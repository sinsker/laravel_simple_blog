@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('layout_top')
    <link href="/css/kanaph/bbs.board.css" rel="stylesheet">
    <link href="/css/kanaph/bbs.post.css" rel="stylesheet">
    <link rel="Stylesheet" type="text/css" href="/lib/smoothDivScroll/css/smoothDivScroll.css" />
@endsection

@section('content')

    {{--

    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                        <li class="active">POST</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    --}}

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb_full">
                    @if (isset($boards))
                        @foreach($boards as $board )
                            <article class="bg_w">
                                <div class="post-image">
                                    <div class="post-heading">
                                        <h3><a href="{{route('bbs.view',['cidx'=> $boardConfig->idx, 'idx'=>$board->idx])}}">{{$board->title}}</a></h3>
                                        <ul class="meta-post">
                                            <li>
                                                <img class="profile-img" src="{{profile_img($board->user_img)}}"  style="width:25px; height:25px;">
                                                {{$board->name}}
                                            </li>
                                            <li><i class="fa fa-2x fa-3x fa-calendar icon-calendar"></i>{{$board->date}}</li>
                                        </ul>
                                    </div>
                                    <br>
                                    {!! $board->src or '' !!}
                                    {{--<div class="video-container">
                                        <iframe src="http://player.vimeo.com/video/30585464?title=0&amp;byline=0">
                                        </iframe>
                                    </div>--}}
                                </div>
                                <p><a href="{{route('bbs.view',['cidx'=> $boardConfig->idx, 'idx'=>$board->idx])}}" style="    color: #222;">
                                        {!! $board->content !!}
                                    </a>
                                </p>
                                <div class="bottom-article">
                                    <ul class="meta-post">
                                        <li><i class="icon-comments"></i><a href="#;">댓글 개수 {{$board->reply_cnt}}개</a></li>
                                        <li>
                                    <span class="like-content" data-idx="{{$board->idx}}">
                                    @if(isset($board->liked) &&$board->liked == true)
                                            <a href="#;" class="liked"><i class="fa fa-heart"></i> <span class="en">좋아요 취소</span></a>
                                        @else
                                            <a href="#:" class="like"><i class="fa fa-heart-o"></i> <span class="en">좋아요</span></a>
                                        @endif
                                    </span>
                                            @if($board->like_cnt > 0)
                                                <span @if(isset($board->liked) &&$board->liked == true) class="liked" @endif style=" font-size: 12px;">
                                        <span class="like-cnt en">&nbsp;(<span>{{$board->like_cnt}}</span>)</span>
                                    </span>
                                            @endif
                                        </li>
                                    </ul>
                                    <a href="{{route('bbs.view',['cidx'=> $boardConfig->idx, 'idx'=>$board->idx])}}" class="pull-right">더 보기<i class="icon-angle-right"></i></a>
                                </div>
                            </article>
                        @endforeach
                    @else
                        글 없음
                    @endif

                    {{--Pagination--}}
                    <div id="pagination">
                        {{--<span class="all">Page ({{$boards->currentPage()}} of {{$boards->lastPage()}})</span>--}}
                        {{$boards->links()}}
                    </div>
                    {{-- END Pagination--}}

                    <div class="buttons">
                        <div class="right-side">
                            @if(session('group')['level'] > 0)
                                <a href="{{route('bbs.create',['cidx'=> $boardConfig->idx])}}" class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i> 글쓰기</a>
                            @endif
                        </div>
                        <br>
                    </div>
                </div>

                <div class="col-lg-4 mb_full">
                    <aside class="">
                        <div class="widget">
                            <div class="form-group">
                                <form class="form-inline" action="{{route('bbs.list',['cidx'=>$boardConfig->idx])}}" method="get" >
                                    <input class="form-control"  type="text" name="word" value="{{$_GET['word'] or ''}}" style="width:280px; display: inline-block;" placeholder="검색 ..." >
                                    <button class="btn btn-default" type="submit" style="padding: 8px;"><i class="fa fa-search"></i></button>
                                </form>
                            </div>

                        </div>

                        <div class="widget">
                            <h5 class="widgetheading">카테고리 분류</h5>
                            <ul class="cat">
                                @if(isset($categories) && count($categories) > 0)
                                    @foreach($categories as $item)
                                        <li><i class="icon-angle-right"></i><a href="#">{{$item->cate}}</a><span> ({{$item->cate_row}})</span></li>
                                    @endforeach
                                @else
                                    없음
                                @endif
                            </ul>
                        </div>

                        <div class="widget">
                            <h5 class="widgetheading">최근 등록된 포스트</h5>
                            <ul class="recent">
                                @if(isset($latestItems) && count($latestItems) > 0)
                                    @foreach($latestItems as $latestItem)
                                        <li style="display: inline-block; width: 100%;">
                                            {!! $latestItem->src or '' !!}
                                            <a href="{{route('bbs.view', ['cidx'=>$boardConfig->idx,'idx'=>$latestItem->idx])}}">
                                                <h6>{{$latestItem->title}} <span style="float: right; color: #b1b1b1; ">{{$latestItem->date}}</span></h6>
                                                <p>
                                                    {!! $latestItem->content or '' !!}
                                                </p>
                                            </a>
                                        </li>
                                    @endforeach
                                @else
                                    없음
                                @endif
                            </ul>
                        </div>

                        <div class="widget">
                            <h5 class="widgetheading">태그</h5>
                            <ul class="tags">
                                @if(isset($tags) && count($tags) > 0)
                                    @foreach($tags as $tag)
                                        <li><a href="{{route('tag.list', ['tag' => $tag->id])}}">{{$tag->id}}</a></li>
                                    @endforeach
                                @else
                                    없음
                                @endif
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('layout_footer')

    <script>

        $('.like').click(function(){

            board_like($(this).parent().data('idx'));
        });

        $('.liked').click(function(){

            board_unlike($(this).parent().data('idx'));
        });

        function board_like(board_idx){

            $.get("/like/"+board_idx+"/love",function(data){

                if(data.like){
                    var love = $('.like-content[data-idx="'+board_idx+'"]');

                    love.html(likeHtmlTemplate('unlike'));
                    love.next().addClass('liked');
                    love.next().find('span.like-cnt > span').text(Number(love.next().find('span.like-cnt> span').text())+1);

                    love.find('.liked').click(function(){
                        board_unlike(board_idx);
                    })
                }

            },"json");
        }


        function board_unlike(board_idx){

            $.get("/like/cancel/"+board_idx,function(data){

                if(data.unlike){
                    var love = $('.like-content[data-idx="'+board_idx+'"]');

                    love.html(likeHtmlTemplate('like'));
                    love.next().removeClass('liked');
                    love.next().find('span.like-cnt > span').text(Number(love.next().find('span.like-cnt> span').text())-1);

                    love.find('.like').click(function(){
                        board_like(board_idx);
                    })
                }

            },"json");

        }


        function likeHtmlTemplate(type)
        {

            var html = '';

            switch (type){

                case 'like':
                    html = '<a href="#:" class="like"><i class="fa fa-heart-o"></i> <span class="en">좋아요</span></a>';
                    break;

                case 'unlike':
                    html = '<a href="#;" class="liked"><i class="fa fa-heart"></i> <span class="en">좋아요 취소</span></a>';
                    break;
            }

            return html;
        }
    </script>
@endsection