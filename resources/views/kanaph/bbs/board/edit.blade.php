@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('btn')
    @if(isset($board->idx))
        수정
    @else
        작성
    @endif
@endsection

@section('action')
    @if(isset($board->idx))
        {{route('bbs.update',['idx'=>$board->idx])}}
    @else
        {{route('bbs.store')}}
    @endif
@endsection

@section('layout_top')
    <link href="/css/board.css" rel="stylesheet">
    <link href="/css/kanaph/bbs.board.css" rel="stylesheet">
@endsection

@section('content')

    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                        <li><a href="#">Board</a><i class="icon-angle-right"></i></li>
                        <li class="active">Notice</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <div class="container" style="padding:  0px;">
        <div class="">
            <br>
            <div class="col-md-12" style="padding:  0px;">
                <div class="panel panel-default" style="margin-bottom:0px;">
                    <div class="panel-body">

                        <form id="procForm" class="form-horizontal"  enctype="multipart/form-data" method="post" action="@yield('action')" onkeydown="return captureReturnKey(event)">
                            {{ csrf_field() }}
                            @if(isset($board->idx))
                                <input name="_method" type="hidden" value="PUT">
                            @endif
                            <input name="bc_idx" type="hidden" value="{{$boardConfig->idx}}">
                            <div class="form-group">
                                <label for="inputWrite" class="col-sm-2 control-label">작성자</label>
                                <div class="col-sm-10">
                                    {{Auth::user()['name']}}
                                    <input type="hidden" class="form-control" id="write" name="write">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputWrite" class="col-sm-2 control-label">카테고리</label>
                                <div class="col-sm-10">
                                    <select name="cate" class="form-control">
                                    <option value="" >- 선택 -</option>
                                    @foreach($boardConfig->cate as $cateItem)
                                        <option value="{{$cateItem}}" @if( isset($board->cate) && $cateItem == $board->cate ) selected="selected" @endif  >{{$cateItem}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-2 control-label">제목</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="title" id="title" value="{{$board->title or ''}}" placeholder="제목">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputContent" class="col-sm-2 control-label">옵션</label>
                                <div class="col-sm-10">
                                    <input class="notice-class" type="checkbox" name="notice" value="1"  @if( isset($board->notice) && $board->notice == 1) checked="checked" @endif >공지사항 &nbsp;</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputContent" class="col-sm-2 control-label">파일첨부</label>
                                <div class="col-sm-10">
                                    <input type="file" name="file">
                                    <!-- 파일 -->
                                    @if(isset($files) && $files->count() >0 )
                                        <div class="attach">
                                            <ul>
                                                @foreach($files as $file)
                                                    <li>
                                                        <i class="fa fa-file"></i>
                                                        <a href="{{route('down.single',['idx'=>$file->idx])}}" title="{{$file->oriname}}" class="en">{{$file->oriname}}</a>
                                                        <span class="size">({{file_size($file->size)}})</span>
                                                        <a class="file-d" href="#;" data-idx="{{$file->idx}}" ><i class="fa fa-times " ></i></a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputContent" class="col-sm-2 control-label">내용</label>
                                <div class="col-sm-10">
                                    <textarea name="content" class="form-control"
                                              rows="3">{{$board->content or ''}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="hidden" name="tag" id="tag" value="{{$board->tag or ''}}">
                                <div class="col-sm-10 col-sm-offset-2">

                                    {{--태그 그룹--}}
                                    <div id="tag-group" class="form-control">
                                        @if(isset($board->tagList))
                                            @foreach($board->tagList as $item)
                                            <span class="tag-item" title="{{$item}}" >
                                            <span>{{$item}}</span>
                                            <span onclick="deleteTag('{{$item}}')" ><i class="fa fa-times" ></i></span>
                                            </span>
                                            @endforeach
                                        @endif
                                        <input type="text" class="form-control" id="add-tag" value="" autocomplete="false" >
                                    </div>

                                    {{--END 태그 그룹--}}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary btn-lg" style="width: 150px;">@yield('btn')</button>
                                    <a href="{{route('bbs.list',['cidx'=>$boardConfig->idx])}}" class="btn btn-default btn-lg">목록</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--이미지 업로드 처리--}}
    <iframe id="_frame_action" name="_frame_action" style="display:none"></iframe>
    <form id="imgForm" action="{{route('upload.board.img')}}" method="post" target="_frame_action"  enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="image" type="file" id="upload" class="hidden" onchange="">
    </form>
    {{--END 이미지 업로드 처리--}}

    <script src="/js/tinymce/tinymce.min.js"></script>
    <script>

        $(function(){
            autoReloadTagContentWrite();
        });

        $('#add-tag').keydown(function (key) {
            if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
                addTag($(this).val());
            }
        });

        $('.file-d').click(function(){
           if(confirm('정말 삭제하시겠습니까?')){

               var data = {
                   '_method' : 'DELETE',
                   '_token' : '{{ csrf_token() }}',
                   'idx' : $(this).data('idx')
               }

               $.post('{{url('/down')}}/'+$(this).data('idx'),data,function(result){

               });

               $(this).parent().remove();
           }
        });

        var date = new Date();
        tinymce.init({
            selector: 'textarea',
            language: "ko_KR",
            height: 300,
            menubar: false,
            plugins: [
                'advlist autolink lists link  charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons  paste textcolor colorpicker textpattern  codesample toc'
            ],
            toolbar: 'undo redo | preview styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | emoticons mybutton',
            setup: function (editor) {

                var input, file;

                $('#_frame_action').on('load',function(){
                    $jsonContent = $(this).contents().find('pre').html();
                    var contact = JSON.parse($jsonContent);
                    editor.insertContent('<img src="/drive/board/' + date.getFullYear() + '/' + leadingZeros((date.getMonth()+1),2) + '/' + contact.image + '" style="width:200px;"/>');
                });

                editor.addButton('mybutton', {

                    icon: "image",
                    onclick: function (e) {

                        $('#upload').trigger('click');
                        $('#upload').change(function () {

                            input = document.getElementById('upload');
                            file = input.files[0];

                            $('#imgForm').submit();
                        });


                    }
                });
            }
        });


        //이미지 삽입
        //opener.tinymce.activeEditor.execCommand("mceInsertContent",'false',이미지경로);

        function leadingZeros(n, digits) {
            var zero = '';
            n = n.toString();

            if (n.length < digits) {
                for (var i = 0; i < digits - n.length; i++)
                    zero += '0';
            }
            return zero + n;
        }

        // files, action URL, response를 받을 callback을 지정


        function addTag(tag){

            if(duplicationTagCheck(tag) == true){
                alert('이미 추가 되어 있는 태그 입니다.');
                return false;
            }

            if(tagCountCheck(3) == true){
                alert('태그를 추가할수 있는 최대 개수를 벗어났습니다.');
                return false;
            }

            tagTemplate(tag);
            autoReloadTagContentWrite();
            inputTagUpdate();

            $('#add-tag').val('');

            $('#imgForm').submit(function( event ) {
                event.preventDefault();
                return false;
            });
        }


        //태그 input 길이 자동 조정
        function autoReloadTagContentWrite(){

            var itemsWidth = 0;

            $('.tag-item').each(function(i){
                itemsWidth += ($(this).width() + 20);
            });

            var tagContent = $('#tag-group').innerWidth() - 40;

            $('#add-tag').css('width', (tagContent - itemsWidth)+'px');
        }

        function tagTemplate(tag){

            $('.tag-item').removeClass('end');
            $('.tag-item').eq($('.tag-item').length-1).addClass('end');


            var html = '<span class="tag-item" title="'+tag+'" >';
                html +='<span>'+tag+'</span>';
                html +='<span onclick="deleteTag(\''+tag+'\')" ><i class="fa fa-times" ></i></span>';
                html +='</span>';

            if($('.tag-item').length > 0) {
                $('.tag-item.end').after(html);
            }else{
                $('#add-tag').before(html);
            }

        }

        function deleteTag(tag){
            $('.tag-item[title="'+tag+'"]').remove();

            autoReloadTagContentWrite();
            inputTagUpdate();
        }

        function captureReturnKey(e) {
            if (e.keyCode == 13 && e.srcElement.type != 'textarea') {
                return false;
            }
        }

        function duplicationTagCheck(tag){

            if($('.tag-item[title="'+tag+'"]').length > 0){
                return true;
            }

            return false;
        }

        function tagCountCheck(count){

            if($('.tag-item').length >= count){
                return true;
            }
            return false;
        }

        function inputTagUpdate(){

            var inputTag = $('#tag');
            var tag = '';

            $('.tag-item').each(function(i){
                tag += $(this).attr('title');

                if(i + 1 != $('.tag-item').length){
                    tag += ',';
                }
            });
            inputTag.val(tag);
        }

    </script>
@endsection
