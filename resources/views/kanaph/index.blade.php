@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('content')
<section id="featured">
    <!-- start slider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Slider -->
                <div id="main-slider" class="flexslider">
                    <ul class="slides">
                        {{--<li>
                            <img src="/images/kanaph/main1.jpg" alt="" />
                            --}}{{--<div class="flex-caption">
                                <h3>KANAPH</h3>
                                <p>저희는 몸짓으로 찬양하는 문화사역 팀입니다.</p>
                                <a href="#" class="btn btn-theme">유투브 보러 가기</a>
                            </div>--}}{{--
                        </li>
                        <li>
                            <img src="/images/kanaph/main2.jpg" alt="" />
                            --}}{{--<div class="flex-caption">
                                <h3>Fully Responsive</h3>
                                <p>Sodales neque vitae justo sollicitudin aliquet sit amet diam curabitur sed fermentum.</p>
                                <a href="#" class="btn btn-theme">Learn More</a>
                            </div>--}}{{--
                        </li>
                        <li>
                            <img src="/images/kanaph/main3.jpg" alt="" />
                            --}}{{--<div class="flex-caption">
                                <h3>Clean & Fast</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit donec mer lacinia.</p>
                                <a href="#" class="btn btn-theme">Learn More</a>
                            </div>--}}{{--
                        </li>
                        --}}
                        <li><a href="/post/2/6">
                            <img src="/images/welcome/_1.jpg" alt="춤추는 예배자로 몸치,박치 실력과 상관없이 하나님에 대한 순수한 열정을 가진 이를 찾습니다.">
                            </a>
                        </li>
                        <li><a href="/post/2/6">

                            <img src="/images/welcome/_2.jpg" alt="춤추는 예배자로 몸치,박치 실력과 상관없이 하나님에 대한 순수한 열정을 가진 이를 찾습니다.">
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- end slider -->
            </div>
        </div>
    </div>

</section>
{{--<section class="">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="big-cta">
                    <div class="cta-text">
                        <h2>Welcome to the <span>KANAPH</span>!</h2>
                        <div class="box-bottom">
                            <a href="/main">메뉴 전체보기</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>--}}
<section class="callaction" id="content" style="padding-bottom: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="box">
                            <div class="box-gray aligncenter">
                                <h4>사역 소개</h4>
                                <div class="icon">
                                    <i class="fa fa-desktop fa-3x"></i>
                                </div>
                                <p>
                                    청년부(SYNC) 스탠딩 예배에서 몸으로 찬양하는 CCD를 진행하고 있습니다.<br>
                                    그 외에도 행사 특송, 스킵드라마등으로<br> 섬기고 있으며 내부적으로 말씀 묵상과 나눔 <br>그리고 지역에 도움을 주는 일을 계획하고 있습니다.
                                </p>

                            </div>
                            <div class="box-bottom" style="background: #424446; padding: 10px 0;">>>
                                <a href="/info">보러가기</a><<
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="box">
                            <div class="box-gray aligncenter">
                                <h4>영상 보기</h4>
                                <div class="icon">
                                    <i class="fa fa-pagelines fa-3x"></i>
                                </div>
                                <p>
                                    팀에서 진행되고 있는 예배 및 각종 행사들을 유투브를 통해 관리하고 있습니다.
                                </p>

                            </div>
                            <div class="box-bottom" style="background: #424446; padding: 10px 0;">>>
                                <a href="https://www.youtube.com/channel/UCI2eFOdtZzV00YT9Y-27T7w" target="_blank">보러가기</a><<
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-lg-3">
                        <div class="box">
                            <div class="box-gray aligncenter">
                                <h4>Customizable</h4>
                                <div class="icon">
                                    <i class="fa fa-edit fa-3x"></i>
                                </div>
                                <p>
                                    Voluptatem accusantium doloremque laudantium sprea totam rem aperiam.
                                </p>

                            </div>
                            <div class="box-bottom">
                                <a href="#">Learn more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="box">
                            <div class="box-gray aligncenter">
                                <h4>Valid HTML5</h4>
                                <div class="icon">
                                    <i class="fa fa-code fa-3x"></i>
                                </div>
                                <p>
                                    Voluptatem accusantium doloremque laudantium sprea totam rem aperiam.
                                </p>

                            </div>
                            <div class="box-bottom">
                                <a href="#">Learn more</a>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
        <!-- end divider -->
        <!-- Portfolio Projects -->
        {{--<div class="row">
            <div class="col-lg-12">
                <h4 class="heading">Recent Works</h4>
                <div class="row">
                    <section id="projects">
                        <ul id="thumbs" class="portfolio">
                            <!-- Item Project and Filter Name -->
                            <li class="col-lg-3 design" data-id="id-0" data-type="web">
                                <div class="item-thumbs">
                                    <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                    <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="춤추는 예배자" href="/images/welcome/_1.jpg">
                                        <span class="overlay-img"></span>
                                        <span class="overlay-img-thumb font-icon-plus"></span>
                                    </a>
                                    <!-- Thumb Image and Description -->
                                    <img src="/images/welcome/_1.jpg" alt="춤추는 예배자로 몸치,박치 실력과 상관없이 하나님에 대한 순수한 열정을 가진 이를 찾습니다.">
                                </div>
                            </li>
                            <!-- End Item Project -->
                            <!-- Item Project and Filter Name -->
                            <li class="item-thumbs col-lg-3 design" data-id="id-1" data-type="icon">
                                <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="사역에 대한 열정" href="/images/welcome/_2.jpg">
                                    <span class="overlay-img"></span>
                                    <span class="overlay-img-thumb font-icon-plus"></span>
                                </a>
                                <!-- Thumb Image and Description -->
                                <img src="/images/welcome/_2.jpg" alt="하나님 앞에선 우리 모두 같은 사람들입니다. 순수한 열정으로 함께 사역할 이를 찾습니다.">
                            </li>

                            <!-- End Item Project -->
                        </ul>
                    </section>
                </div>
            </div>
        </div>--}}

    </div>
</section>
@endsection