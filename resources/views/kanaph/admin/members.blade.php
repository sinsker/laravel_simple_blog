@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('content')

    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-success">
                    <table class="table table-hover">
                        <tr onclick="javascript:location.href='{{route('admin.kanaph.member')}}'">
                            <td class="text-center active">사용자관리</td>
                        </tr>
                        <tr onclick="javascript:location.href='{{route('admin.kanaph')}}'">
                            <td class="text-center ">게시판관리</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">회원 리스트</div>

                    <div class="panel-body">
                        <table class="table table-hover">
                            <tr>
                                <td class="text-center">이름</td>
                                <td class="text-center">그룹 / 권한</td>
                                <td class="text-center">Email</td>
                                <td class="text-center">phone</td>
                                <td class="text-center">최종접속날짜</td>
                                <td class="text-center">가입날짜</td>
                            </tr>
                            @if (isset($configList))
                                @foreach($configList as $item )
                                    <tr onclick="javascript:location.href='{{route('admin.kanaph.member.view',['idx'=>$item->idx] )}}'">
                                        <td class="text-center">{{$item->name}}</td>
                                        <td class="text-center">{{$config['part'][$item->part]['info'].'/'.$config['group'][$item->group_idx]}}</td>
                                        <td class="text-center">{{$item->email}}</td>
                                        <td class="text-center">{{$item->phone}}</td>
                                        <td class="text-center">{{$item->access_at}}</td>
                                        <td class="text-center">{{$item->created_at}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="5">사용자가 없습니다.</td>
                                </tr>
                            @endif

                        </table>

                    </div>
                </div>
            </div>
        </div>
        @if(isset($user->idx))
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading" data-toggle="collapse" href="#addConfig" style="cursor:pointer;"> + 수정</div>

                    <div class="panel-body panel-collapse @if(!isset($user->idx)) collapse @endif" id="addConfig">
                        <form id="procForm" class="form-horizontal" method="post" action="{{route('admin.kanaph.member.update',['idx'=>$user->idx])}} ">
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value="PUT">
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-2 control-label">이름</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name" id="bc_name" value="{{$user->name or ''}}" placeholder="게시판 이름">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-2 control-label">전화번호</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="phone" id="bc_cate" value="{{$user->phone or ''}}" placeholder=",로 구분하여 작성해주세요.">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-2 control-label">권한</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="group" id="bc_row">
                                        @foreach($config['group'] as $key => $val)
                                            <option value="{{$key}}" @if( isset($user) && $key == $user->group_idx ) selected="selected" @endif >{{$val}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-2 control-label">파트</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="part" id="bc_row">
                                        @foreach($config['part'] as $key => $val)
                                            <option value="{{$key}}" @if( isset($user) && $key == $user->part ) selected="selected" @endif >{{$val['info']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">확인</button>
                                    <a href="{{route('admin.kanaph.member')}}" class="btn btn-default">초기화면</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection