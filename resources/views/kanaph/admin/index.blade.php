@extends('kanaph.extends.layout')
@extends('kanaph.extends.top')
@extends('kanaph.extends.footer')

@section('content')

    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-success">
                    <table class="table table-hover">
                        <tr onclick="javascript:location.href='{{route('admin.kanaph.member')}}'">
                            <td class="text-center ">사용자관리</td>
                        </tr>
                        <tr onclick="javascript:location.href='{{route('admin.kanaph')}}'">
                            <td class="text-center active">게시판관리</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">게시판 종류</div>

                    <div class="panel-body">
                        <table class="table table-hover">
                            <tr>
                                <td class="text-center">#BOARD ID</td>
                                <td class="text-center">이름</td>
                                <td width="20%" class="text-center">카테고리</td>
                                <td class="text-center">타입</td>
                            </tr>
                            @if (isset($configList))
                                @foreach($configList as $item )
                                    <tr onclick="javascript:location.href='{{route('admin.kanaph.view',['idx'=>$item->idx] )}}'">
                                        <td class="text-center">{{$item->idx}}</td>
                                        <td class="text-center">{{$item->bc_name}}</td>
                                        <td class="text-center">{{$item->bc_cate}}</td>
                                        <td class="text-center">{{$config['type'][$item->bc_type]['name']}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="5">게시판이 없습니다.</td>
                                </tr>
                            @endif

                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading" data-toggle="collapse" href="#addConfig" style="cursor:pointer;"> + 추가</div>

                    <div class="panel-body panel-collapse @if(!isset($view->idx)) collapse @endif" id="addConfig">
                        <form id="procForm" class="form-horizontal" method="post" action="@if(isset($view->idx)) {{route('admin.kanaph.update',['idx'=>$view->idx])}} @else {{route('admin.kanaph.store')}} @endif">
                            {{ csrf_field() }}
                            @if(isset($view->idx))
                                <input name="_method" type="hidden" value="PUT">
                            @endif
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-2 control-label">게시판 이름</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="bc_name" id="bc_name" value="{{$view->bc_name or ''}}" placeholder="게시판 이름">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-2 control-label">카테고리</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="bc_cate" id="bc_cate" value="{{$view->bc_cate or ''}}" placeholder=",로 구분하여 작성해주세요.">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-2 control-label">게시글 수</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="bc_row" id="bc_row">
                                        @foreach($config['row'] as $val)
                                            <option value="{{$val}}" @if( isset($view) && $val == $view->bc_row ) selected="selected" @endif >{{$val}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-2 control-label">타입</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="bc_type" id="bc_type">
                                        @foreach($config['type'] as $key => $val)
                                            <option value="{{$key}}" @if( isset($view) && $view->bc_type == $key) selected="selected" @endif >{{$val['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputTitle" class="col-sm-2 control-label">설명</label>
                                <div class="col-sm-10">
                                    <textarea name="bc_info" class="form-control" rows="3">{{$view->bc_info or ''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">확인</button>
                                    <a href="{{route('admin.kanaph')}}" class="btn btn-default">초기화면</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection