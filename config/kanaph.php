<?php

return [

    'part' => [
        'D' =>[
            'name'=>'Non Part',
            'info'=>'미정 상태'
        ],
        'CR' =>[
            'name'=>'Creative Part',
            'info'=>'크리에이티브 파트'
            ],
        'M' =>[
            'name'=>'Mission Part',
            'info'=>'미션 파트'
        ],
        'CM' =>[
            'name'=>'Community Part',
            'info'=>'커뮤니티 파트'
        ],
    ],

    'group' => [
        '0' => '손님',
        '1' => '팀원',
        '2' => '파트원',
        '3' => '파트장',
        '5' => '팀장',
        '9' => '관리자',
        '10' => '최고관리자',
    ],


    'week' => ['일','월','화','수','목','금','토'],


    'kakao' => ['key' => '5c4dc52100bb1ea045aed87ea749829a'],
    'facebook' => ['key' => '290203064748942']


];