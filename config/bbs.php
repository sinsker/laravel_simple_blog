<?php

return [

    //게시판 검색
    'search' =>[
        'title'     => '제목',
        'content'   => '내용',
        'writer'    => '작성자'
    ],

    //게시판 타입
    'type' =>[
        '0' => ['view'=>'board', 'name' =>'게시판 형식'],
        '1' =>  ['view'=>'post', 'name' =>'블로그 형식']
    ],

    //게시물 개수
    'row' => [
        5,
        10,
        15,
        20,
        30,
        35,
        40
    ]

];